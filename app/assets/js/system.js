editor = null;

function initializeSystem() {
  editor = CodeMirror(document.getElementById('editor'), {
    mode: 'text/x-java',
    lineNumbers: true,
    theme: 'base16-light'
  });
}
