/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': 'login',
  '/login': 'login',
  '/flgn': 'flogin',
  '/tasks': 'tasks',
  '/solve': 'solve',
  '/submit': 'submit',
  '/past': 'past',
  '/a': 'admin',
  '/a/account': 'account/account',
  '/a/task': 'task/task',
  '/a/submission': 'submission/submission',
  '/a/submission/custom': 'submission/custom',
  'post /a/submission/download': 'submission/download',
  '/a/submission/excel': 'submission/excel',
  'post /a/account/add': 'account/add',
  'post /a/account/batch': 'account/batch',
  'post /a/account/deactivate': 'account/deactivate',
  'post /a/account/delete': 'account/delete',
  'post /a/task/add': 'task/add',
  'post /a/task/delete': 'task/delete',
  'post /a/task/toggle': 'task/toggle',
  'post /a/task/pasttoggle': 'task/pasttoggle',
  '/logout': 'logout',
  '/account': {response: 'notFound'},
  '/task': {response: 'notFound'},
  '/faculty': {response: 'notFound'},
  '/submission': {response: 'notFound'}

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
