module.exports = {
  friendlyName: 'Submit Answer',
  description: 'Lets the user submit an answer for the problem.',
  inputs: {
    userName: {
      description: 'The user name.',
      type: 'string'
    },
    taskId: {
      description: 'The task ID in the database.',
      type: 'string'
    },
    response: {
      description: 'The answer to be submitted.',
      type: 'json'
    }
  },
  exits: {
    accountAuthorized: {
      description: 'Account is authorized and submission was made successfully.',
      responseType: 'view',
      viewTemplatePath: 'pages/submit'
    },
    accountUnauthorized: {
      description: 'Account is not authorized and submission cannot be made.',
      responseType: 'redirect'
    },
    submissionAlreadyExists: {
      description: 'A submission by the given user for the given problem already exists and a new one cannot be made.',
      responseType: 'view',
      viewTemplatePath: 'pages/submit'
    },
    problemIsNotAvailable: {
      description: 'Submission for the problem is currently restricted.',
      responseType: 'view',
      viewTemplatePath: 'pages/submit'
    },
    submissionError: {
      description: 'An error occurred while submitting the code.',
      responseType: 'view',
      viewTemplatePath: 'pages/submit'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.accountUnauthorized('login');
      return;
    }
    // Checks if the currently logged in user is a student (std = student)
    else if (this.req.session.code != 'std') {
      exits.accountUnauthorized('login');
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Get the specified user from the database
    var user = await Account.findOne({where: {user_name: inputs.userName}});
    if (!user || user.status == 'inactive') {
      // If someone tried to submit from an inactive account, or tried to submit using a non-existent user, immediately logout
      exits.accountUnauthorized('logout');
      return;
    }

    // Get the specified task to which the submission is being made from the database
    var task = await Task.findOne({where: {id: inputs.taskId}});
    if (!task) {
      // If the task does not exist in the datanase, respond with error
      exits.problemIsNotAvailable({locale: locale, type: 'problem-not-available', user_name: this.req.session.userName});
      return;
    }
    if (task.live == 'no') {

      // If the task is not live (i.e., not accepting submissions), respond with error
      exits.problemIsNotAvailable({locale: locale, type: 'problem-not-available', user_name: this.req.session.userName});
      return;
    }

    // Get all submissions for the specified task made by the current user
    var submissions = await Submission.find({where: {user: user.id, task: task.id}});
    // If there is already an existing submission, it must be updated
    if (submissions.length != 0) {
      var submission = await Submission.findOne({where: {user: user.id, task: task.id}});

      // Check if the response is valid based on the task type.
      var responseValid = true;
      for (var  i = 0; i < inputs.response.length; i++) {
        if (inputs.response[i].trim() == '') {
          responseValid = false;
          break;
        }
      }
      if (task.type == 'function' && inputs.response.length != 1) {
        responseValid = false;
      }
      else if (task.type == 'answer' && inputs.response.length != task.question_texts.length) {
        responseValid = false;
      }
      if (!responseValid) {
        exits.submissionError({locale: locale, type: 'submission-error', user_name: this.req.session.userName});
        return;
      }

      // Update the submission
      await Submission.update({where: {user: user.id, task: task.id}}).set({response: inputs.response});
      exits.accountAuthorized({locale: locale, type: 'submission-successful', taskRef: task.id, user_name: this.req.session.userName});
      return;
    }

    // Otherwise if there is no submission yet, then a new one must be created
    if (inputs.response.length == 0) {
      exits.submissionError({locale: locale, type: 'submission-error', user_name: this.req.session.userName});
      return;
    }

    // Check if the response is valid based on the task type.
    var responseValid = true;
    for (var  i = 0; i < inputs.response.length; i++) {
      if (inputs.response[i].trim() == '') {
        responseValid = false;
        break;
      }
    }
    if (task.type == 'function' && inputs.response.length != 1) {
      responseValid = false;
    }
    else if (task.type == 'answer' && inputs.response.length != task.question_texts.length) {
      responseValid = false;
    }
    if (!responseValid) {
      exits.submissionError({locale: locale, type: 'submission-error', user_name: this.req.session.userName});
      return;
    }

    // Update the submission
    var result = await Submission.create({user: user.id, task: task.id, response: inputs.response});
    exits.accountAuthorized({locale: locale, type: 'submission-successful', taskRef: task.id, user_name: this.req.session.userName});
  }
}
