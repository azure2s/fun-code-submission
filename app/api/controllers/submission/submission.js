module.exports = {
  friendlyName: 'Manage Submissions',
  description: 'Allows the admin to manage submissions.',
  inputs: {
    taskId: {
      description: 'if present, view the submission for this specific task (if not, display all tasks)',
      type: 'string'
    },
    userId: {
      description: 'if present (and task ID is absent), view the submission for this specific user',
      type: 'string'
    },
    submissionId: {
      description: 'if present, view the specific submission (overrides taskId and userId)',
      type: 'string'
    },
    referrer: {
      description: 'if viewing a submission, specified whether the user came from the task or user list',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedView: {
      description: 'Account is authorized and task list can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/list'
    },
    adminAuthorizedViewTask: {
      description: 'Account is authorized and submissions for the task can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/submission'
    },
    adminAuthorizedViewUser: {
      description: 'Account is authorized and submissions for the user can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/submission'
    },
    adminAuthorizedViewSubmission: {
      description: 'Account is authorized and the submission can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/examine'
    },
    invalidTaskId: {
      description: 'Invalid task ID is provided.',
      responseType: 'notFound'
    },
    invalidUserId: {
      description: 'Invalid user ID is provided.',
      responseType: 'notFound'
    },
    invalidSubmissionId: {
      desciption: 'Invalid submission ID is provided.',
      responseType: 'notFound'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If userId, taskId, and submissionId are not provided, render all tasks and users in the view submissions page
    if (!inputs.userId && !inputs.taskId && !inputs.submissionId) {
      tasks = await Task.find({select: ['id', 'title', 'createdAt'], where: {status: 'active'}});
      accounts = await Account.find({select: ['id', 'user_name', 'name', 'createdAt'], where: {status: 'active'}})
      exits.adminAuthorizedView({locale: locale, tasks: tasks, accounts: accounts})
      return;
    }
    // Else, if submissionId is provided, render the specific submission in the examine submission page
    else if (inputs.submissionId) {
      submission = await Submission.findOne({id: inputs.submissionId}).populate('user').populate('task');
      if (!submission) {
        exits.invalidSubmissionId();
        return;
      }
      submission.task = await sails.helpers.preprocessTask.with({task: submission.task});
      submission = await sails.helpers.preprocessSubmission.with({submission: submission});
      referrer = inputs.referrer;
      exits.adminAuthorizedViewSubmission({locale: locale, submission:submission, task: submission.task, referrer: inputs.referrer});
      return;
    }
    // Else, if taskId is provided, render all submissions made for that task in the view submissions page
    else if (inputs.taskId) {
      task = await Task.findOne({where: {id: inputs.taskId}});
      if (!task) {
        exits.invalidTaskId();
        return;
      }
      task = await sails.helpers.preprocessTask.with({task: task});
      submissions = await Submission.find({where: {task: task.id}}).populate('user');
      exits.adminAuthorizedViewTask({locale: locale, user: undefined, task: task, submissions: submissions});
      return;
    }
    // Else, if userId is provided, render all submissions made by that user in the view submissions page
    else if (inputs.userId) {
      user = await Account.findOne({where: {id: inputs.userId}});
      if (!user) {
        exits.invalidUserId();
        return;
      }
      submissions = await Submission.find({where: {user: user.id}}).populate('task');
      exits.adminAuthorizedViewUser({locale: locale, task: undefined, user: user, submissions: submissions});
      return;
    }
    // Otherwise, respond with error
    exits.invalidSubmissionId();
    return;
  }
}
