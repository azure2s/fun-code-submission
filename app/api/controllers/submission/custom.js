module.exports = {
  friendlyName: 'Display Custom Download Menu',
  description: 'Allows the admin to select users to download for a specific problem.',
  inputs: {
    taskId: {
      description: 'select users to download for this specific task.',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedCustomDownload: {
      description: 'Account is authorized and custom download can be performed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/custom'
    },
    invalidTaskId: {
      description: 'Invalid task ID is provided.',
      responseType: 'notFound'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Get the task with the given task ID from the database
    task = await Task.findOne({where: {id: inputs.taskId}});

    // If the task does not exist, respond with error
    if (!task) {
      exits.invalidTaskId();
      return;
    }

    // Preprocess the task for displaying on the HTML
    task = await sails.helpers.preprocessTask.with({task: task});

    // Get all submissions for that task
    submissions = await Submission.find({where: {task: task.id}}).populate('user');

    // Render the custom download page
    exits.adminAuthorizedCustomDownload({locale: locale, user: undefined, task: task, submissions: submissions});
    return;
  }
}
