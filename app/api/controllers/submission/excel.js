const fs = require('fs');
const zipFolder = require('folder-zip-sync')
const sanitize = require("sanitize-filename");
const alphaSort = require('alpha-sort');
const Excel = require('exceljs')

module.exports = {
  friendlyName: 'Download Raw Excel of Submissions',
  description: 'Allows the admin to download all submissions for a specific task as raw Excel.',
  inputs: {
    o: {
      description: 'option (a - download all, t - download for a problem, ct - custom download for a problem, u - download for a user, defaults to a)',
      type: 'string'
    },
    taskId: {
      description: 'if o = t or o = ct, download submissions for the task with the given task ID.',
      type: 'string'
    },
    userId: {
      description: 'if o = u, download all the submissions for the user with the given user ID.',
      type: 'string'
    },
    userList: {
      description: 'list of users to download.',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedDownload: {
      description: 'Account is authorized and the submissions will be downloaded.',
      statusCode: 200
    },
    invalidOption: {
      description: 'Invalid option provided.',
      statusCode: 200
    },
    invalidTaskId: {
      description: 'Invalid task ID is provided.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }

    // If there is no option provided, respond with error
    if (!inputs.o) {
      exits.invalidOption({result: 'invalid'});
      return;
    }

    var tasks = null;
    var users = null;

    if (inputs.o == 'a') {
      // If option = a (all), get all active tasks and all active users from the database
      tasks = await Task.find({status: 'active'});
      users = await Account.find({status: 'active'});
    }
    else if (inputs.o == 't') {
      // If option  = t (task), get all users and select the specified task, or respond with error if there is no taskId given
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({id: inputs.taskId});
      if (tasks.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      users = await Account.find({status: 'active'});
    }
    else if (inputs.o == 'u') {
      // If option = u (user), get all tasks and select the specified user, or respond with error if there no userId given
      if (!inputs.userId || inputs.userId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      users = await Account.find({id: inputs.userId});
      if (accounts.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({status: 'active'});
    }
    else if (inputs.o == 'ct') {
      // If option = ct (custom task), get the specified task and select list of users provided, or respond with error if any of these details are missing
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      if (!inputs.userList || inputs.userList.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({id: inputs.taskId});
      if (tasks.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      var list = inputs.userList.split('\n');
      for (var i = 0; i < list.length; i++) {
        list[i] = list[i].trim();
      }
      users = await Account.find({status: 'active', user_name: {'in': list}});
    }
    else {
      // Otherwise, respond with error because the option is invalid
      exits.invalidOption({result: 'invalid'});
      return;
    }

    // Generate the Excel file in the tempDir directory defined in config/custom.js
    // The name of the directory is a unique identifier generated from the current timestamp
    var tempDir = sails.config.custom.tempDir;
    var timestamp = Math.floor(Date.now());
    while (fs.existsSync(tempDir + timestamp)) {
      timestamp++;
    }
    fs.mkdirSync(tempDir + timestamp);

    var taskIds = tasks.map(a => a.id);
    var taskTitles = tasks.map(a => a.title);
    var userIds = users.map(a => a.id);
    var userNames = users.map(a => a.user_name);

    for (var i = 0; i < userNames.length; i++) {
      for (var j = i + 1; j < userNames.length; j++) {
        if (userNames[i].localeCompare(userNames[j]) > 0) {
          var temp = userNames[i];
          userNames[i] = userNames[j];
          userNames[j] = temp;
          temp = userIds[i];
          userIds[i] = userIds[j];
          userIds[j] = temp;
        }
      }
    }

    // Get all submissions made by users in the list to tasks in the list from the database
    var submissions = await Submission.find({where: {task: {'in': taskIds}, user: {'in': userIds}}}).populate('user').populate('task');

    // Generate the Excel file containing the raw submissions
    var workbook = new Excel.Workbook();
    workbook.creator = 'funcodesubmission';
    workbook.created = new Date();
    for (var i = 0; i < taskTitles.length; i++) {
      var currentSheet = workbook.addWorksheet((i + 1) + ' - ' + taskTitles[i], {properties: {defaultColWidth: 23}});
      currentSheet.addRow(['USER_NAME', 'SUBMISSION'])
      var rowData = [];
      for (var j = 0; j < userIds.length; j++) {
        rowData.push([userNames[j], 'no submission']);
      }


      for (var j = submissions.length - 1; j >= 0; j--) {
        if (submissions[j].task.id == taskIds[i]) {
          var targetIndex =  userIds.indexOf(submissions[j].user.id);
          var temp = submissions[j].response;
          rowData[targetIndex][1] = temp.join('\r\n');
        }
      }

      for (var j = 0; j < rowData.length; j++) {
        currentSheet.addRow(rowData[j]);
      }
      for (var j = 0; j < userIds.length + 1; j++) {
        var row1 = currentSheet.getRow(j + 1);
        row1.getCell(1).alignment = { wrapText: true }
        row1.getCell(2).alignment = { wrapText: true }
      }
    }

    try {
      // Write the Excel file to the created directory
      await workbook.xlsx.writeFile(tempDir + timestamp + '/' + 'submissions.xlsx');
    }
    catch(e) {
      throw new Error(e);
    }

    // Initiate download of Excel file
    this.res.attachment('submissions.xlsx');
    var downloading = await sails.startDownload(tempDir + timestamp + '/' + 'submissions.xlsx');
    return exits.success(downloading);
  }
}
