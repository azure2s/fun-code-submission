const fs = require('fs');
const zipFolder = require('folder-zip-sync')
const sanitize = require("sanitize-filename");

module.exports = {
  friendlyName: 'Download Submissions',
  description: 'Allows the admin to download all submissions for a specific task.',
  inputs: {
    o: {
      description: 'option (a - download all, t - download for a problem, ct - custom download for a problem, u - download for a user, defaults to a)',
      type: 'string'
    },
    taskId: {
      description: 'if o = t or o = ct, download submissions for the task with the given task ID.',
      type: 'string'
    },
    userId: {
      description: 'if o = u, download all the submissions for the user with the given user ID.',
      type: 'string'
    },
    userList: {
      description: 'list of users to download.',
      type: 'string'
    },
    check: {
      description: 'If present and "yes", check the submissions first (only for options t, ct, or u and unraw)',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedDownload: {
      description: 'Account is authorized and the submissions will be downloaded.',
      statusCode: 200
    },
    invalidOption: {
      description: 'Invalid option provided.',
      statusCode: 200
    },
    invalidTaskId: {
      description: 'Invalid task ID is provided.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }

    // If there is no option provided, respond with error
    if (!inputs.o) {
      exits.invalidOption({result: 'invalid'});
      return;
    }

    var tasks = null;
    var users = null;

    if (inputs.o == 'a') {
      // If option = a (all), get all active tasks and all active users from the database
      tasks = await Task.find({status: 'active'});
      users = await Account.find({status: 'active'});
    }
    else if (inputs.o == 't') {
      // If option  = t (task), get all users and select the specified task, or respond with error if there is no taskId given
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({id: inputs.taskId});
      if (tasks.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      users = await Account.find({status: 'active'});
    }
    else if (inputs.o == 'u') {
      // If option = u (user), get all tasks and select the specified user, or respond with error if there no userId given
      if (!inputs.userId || inputs.userId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      users = await Account.find({id: inputs.userId});
      if (accounts.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({status: 'active'});
    }
    else if (inputs.o == 'ct') {
      // If option = ct (custom task), get the specified task and select list of users provided, or respond with error if any of these details are missing
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      if (!inputs.userList || inputs.userList.trim() == '') {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      tasks = await Task.find({id: inputs.taskId});
      if (tasks.length == 0) {
        exits.invalidOption({result: 'invalid'});
        return;
      }
      var list = inputs.userList.split('\n');
      for (var i = 0; i < list.length; i++) {
        list[i] = list[i].trim();
      }
      users = await Account.find({status: 'active', user_name: {'in': list}});
    }
    else {
      // Otherwise, respond with error because the option is invalid
      exits.invalidOption({result: 'invalid'});
      return;
    }

    // Get the IDs of all selected tasks and users
    var taskIds = tasks.map(a => a.id);
    var userIds = users.map(a => a.id);

    // Get all submissions made by users in the list to tasks in the list from the database
    var submissions = await Submission.find({where: {task: {'in': taskIds}, user: {'in': userIds}}}).populate('user').populate('task');

    if (inputs.o == 't' || inputs.o == 'ct' || inputs.o == 'u') {
      // If automatic checking is selected, perform checking
      if (inputs.check && inputs.check == 'yes') {
        submissions = await sails.helpers.checkSubmissions.with({submissions: submissions});
      }
    }

    // Respond with the JavaScript object containing the users, tasks, and submissions
    var result = {users: users, tasks: tasks, submissions: submissions};
    exits.adminAuthorizedDownload({result: 'success', submissions: result});
    return;
  }
}
