module.exports = {
  friendlyName: 'View Past Submission',
  description: 'Allows the students to view a past submission.',
  inputs: {
    submissionId: {
      description: 'if present, view the specific submission',
      type: 'string'
    }
  },
  exits: {
    accountAuthorized: {
      description: 'Account is authorized and tasks can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/submission/examine'
    },
    accountUnauthorized: {
      description: 'Account is not authorized and tasks cannot be viewed.',
      responseType: 'redirect'
    },
    invalidSubmissionId: {
      description: 'Provided submission ID is invalid.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.accountUnauthorized('login');
      return;
    }
    // Checks if the currently logged in user is a student (std = student)
    else if (this.req.session.code != 'std') {
      exits.accountUnauthorized('login');
      return;
    }

    // Check if the currently logged in user is still active in the database
    var currentUser = await Account.findOne({where: {'user_name': this.req.session.userName}});
    if (!currentUser || currentUser.status == 'inactive') {
      exits.accountUnauthorized('login');
      return;
    }

    // Get the specified submission from the database
    var currentUserId = currentUser.id;
    submission = await Submission.findOne({id: inputs.submissionId}).populate('user').populate('task');

    // If the specified submission is not the database, respond with error
    if (!submission) {
      exits.invalidSubmissionId();
      return;
    }

    // If the specified subsmission is not made the currently logged in user, respond with error as well (cannot view other stduent's submissions)
    if (submission.user.id != currentUserId) {
      exits.invalidSubmissionId();
      return;
    }

    // Check if past submissions are visible or note
    var pastVisible = false;
    var vSetting = await Setting.findOne({settingkey: 'past'});
    if (!vSetting) {
      pastVisible = false;
    }
    else if (vSetting.settingvalue == 'true') {
      pastVisible = true;
    }
    else {
      pastVisible = false;
    }

    if (!pastVisible) {
      exits.invalidSubmissionId();
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Preprocess the task object to prepare it for displaying it in HTML
    submission.task = await sails.helpers.preprocessTask.with({task: submission.task});

    // Preprocess the submission object to prepare it for displaying it in HTML
    submission = await sails.helpers.preprocessSubmission.with({submission: submission});

    // Set the referrer to student-view to tell renderer in pages/submission/examine.ejs that the page is to be rendered as a student view
    referrer = 'student-view';

    // Render the submission examine page
    exits.accountAuthorized({locale: locale, submission:submission, task: submission.task, referrer: referrer, user_name: this.req.session.userName});
    return;
  }
}
