module.exports = {
  friendlyName: 'Deactivate Account',
  description: 'Deactivates a single account in the database.',
  inputs: {
    userName: {
      description: 'The user name of the account to be added.',
      type: 'string'
    }
  },
  exits: {
    accountDeactivated: {
      description: 'Account was successfully deactivated.',
      statusCode: 200
    },
    deactivateError: {
      description: 'An error occurred and the account was not deactivated.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }

    // Load the texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If user name is empty or non-existent, respond with error
    if (inputs.userName.trim() == '') {
      exits.deactivateError({result: 'missing'});
      return;
    }

    // Update the status of the account in the database to deactivate it
    try {
      var updated = await Account.update({user_name: inputs.userName}).set({status: 'inactive'}).fetch();
      if (updated.length == 0) {
        exits.deactivateError({result: 'failed'})
        return;
      }
    } catch(error) {
      exits.deactivateError({result: 'failed'})
      return;
    }

    exits.accountDeactivated({result: 'success'});
  }
}
