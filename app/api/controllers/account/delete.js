module.exports = {
  friendlyName: 'Delete Account',
  description: 'Deletes a single account and all associated submissions in the database.',
  inputs: {
    userName: {
      description: 'The user name of the account to be added.',
      type: 'string'
    }
  },
  exits: {
    accountDeleted: {
      description: 'Account was successfully deactivated.',
      statusCode: 200
    },
    deleteError: {
      description: 'An error occurred and the account was not deactivated.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If user name is empty or non-existent, respond with error
    if (inputs.userName.trim() == '') {
      exits.deleteError({result: 'missing'});
      return;
    }

    // Delete the user and all associaed submissions from the database (this action cannot be reversed)
    try {
      var account = await Account.destroyOne({user_name: inputs.userName});
      if (!account) {
        exits.deleteError({result: 'failed'})
        return;
      }
      else {
        try {
          await Submission.destroy({user: account.id});
        }
        catch(error) {
          exits.deleteError({result: 'failed'})
          return;
        }
      }
    }
    catch(error) {
      exits.deleteError({result: 'failed'})
      return;
    }

    exits.accountDeleted({result: 'success'});
  }
}
