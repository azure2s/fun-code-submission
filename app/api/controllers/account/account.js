module.exports = {
  friendlyName: 'Manage Accounts',
  description: 'Allows the admin to manage accounts.',
  inputs: {
    o: {
      description: 'option on what action should be made (a=add, b=batch add, v=view) which defaults to v.',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedView: {
      description: 'Account is authorized and accounts can be managed.',
      responseType: 'view',
      viewTemplatePath: 'pages/account/account'
    },
    adminAuthorizedAdd: {
      description: 'Account is authorized and an account can be added.',
      responseType: 'view',
      viewTemplatePath: 'pages/account/add'
    },
    adminAuthorizedBatch: {
      description: 'Account is authorized and an account can be added by batch.',
      responseType: 'view',
      viewTemplatePath: 'pages/account/batch'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // if there is no option provided, default to v (view)
    if (!inputs.o) {
      inputs.o = 'v';
    }

    // Load the texts to display
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If option is a (add), render the add account page
    if (inputs.o == 'a') {
      exits.adminAuthorizedAdd({locale: locale});
      return;
    }
    // If option is b (batch add), render the batch add account page
    else if (inputs.o == 'b') {
      exits.adminAuthorizedBatch({locale: locale});
      return;
    }

    // Otherwise, render the view accounts page
    // Get all accounts from database
    var accounts = await Account.find({select: ['name', 'user_name', 'password', 'status', 'createdAt']});
    exits.adminAuthorizedView({locale: locale, accounts: accounts});
  }
}
