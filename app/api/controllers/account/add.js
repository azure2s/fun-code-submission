module.exports = {
  friendlyName: 'Add Account',
  description: 'Adds a single account to the database.',
  inputs: {
    name: {
      description: 'The name associated with the account.',
      type: 'string'
    },
    userName: {
      description: 'The user name of the account to be added.',
      type: 'string'
    },
    password: {
      description: 'The password of the account to be added.',
      type: 'string'
    }
  },
  exits: {
    accountAdded: {
      description: 'Account was successfully added.',
      responseType: 'redirect'
    },
    addError: {
      description: 'An error occurred and the account was not added.',
      responseType: 'view',
      viewTemplatePath: 'pages/account/add'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load the texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If some fields are empty or non-existent, respond with error
    if (inputs.name.trim() == '') {
      exits.addError({locale: locale, error: 'missing', responses: inputs});
      return;
    }
    if (inputs.userName.trim() == '') {
      exits.addError({locale: locale, error: 'missing', responses: inputs});
      return;
    }
    if (inputs.password.trim() == '') {
      exits.addError({locale: locale, error: 'missing', responses: inputs});
      return;
    }

    // Check if account with the same user name already exists
    var test = await Account.findOne({user_name: inputs.userName});
    if (test) {
      exits.addError({locale: locale, error: 'duplicate', details: inputs.userName, responses: inputs});
      return;
    }

    // Add the new account to the database
    try {
      await Account.create({name: inputs.name, user_name: inputs.userName, password: inputs.password, status: 'active'});
    } catch(error) {
      exits.addError({locale: locale, error: 'failed', details: error, responses: inputs})
      return;
    }

    // Render the view accounts page
    exits.accountAdded(sails.config.custom.baseUrl + 'a/account');
  }
}
