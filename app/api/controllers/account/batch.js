module.exports = {
  friendlyName: 'Batch Add Account',
  description: 'Adds multiple accounts to the database via a batch file.',
  inputs: {
    accountList: {
      description: 'A list of accounts to add in a CSV format.',
      type: 'string'
    }
    /* BATCH FILE FORMAT */
    // name,user name,password
    // name,user name,password
    // name,user name,password
    // name,user name,password
    // name,user name,password
    /* END OF BATCH FILE FORMAT */
  },
  exits: {
    accountsAdded: {
      description: 'Accounts were all successfully added.',
      responseType: 'redirect'
    },
    addError: {
      description: 'An error occurred and the accounts were not added.',
      responseType: 'view',
      viewTemplatePath: 'pages/account/batch'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user 
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load the texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // If account list is empty or non-existent, respond with error
    if (inputs.accountList.trim() == '') {
      exits.addError({locale: locale, error: 'missing', responses: inputs});
      return;
    }


    try {
      // Parse the account list, returning erros if format is incorrect
      var list = inputs.accountList.trim().split('\r\n');
      var toCreate = [];
      var ids = [];
      var doubles = [];
      for (var i = 0; i < list.length; i++) {
        if (list[i].trim() == '') continue;
        var split = list[i].split(',');
        if (split.length != 3) {
          exits.addError({locale: locale, error: 'format', details: (i + 1) + ': ' + list[i], responses: inputs});
          return;
        }
        var obj = {name: split[0].trim(), user_name: split[1].trim(), password: split[2].trim(), status: 'active'};
        toCreate.push(obj);
        if (ids.indexOf(split[1].trim()) != -1) {
          if (doubles.indexOf(split[1]) == -1) {
            doubles.push(split[1]);
          }
        }
        ids.push(split[1].trim());
      }
      if (doubles.length > 0) {
        exits.addError({locale: locale, error: 'double', details: doubles, responses: inputs})
        return;
      }
      // Check if some user names already exist in the database
      var test = await Account.find({where: {'user_name': {'in': ids}}});
      if (test.length > 0) {
        var conflicts = test.map(a => a.user_name);
        exits.addError({locale: locale, error: 'duplicate', details: conflicts, responses: inputs})
        return;
      }
      // Add all accounts to the database
      var result = await Account.createEach(toCreate);
    } catch(error) {
      exits.addError({locale: locale, error: 'failed', responses: inputs})
    }

    // Render the view accounts page
    exits.accountsAdded(sails.config.custom.baseUrl + 'a/account');
  }
}
