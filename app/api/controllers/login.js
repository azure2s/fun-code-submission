module.exports = {
  friendlyName: 'Login',
  description: 'Login to the specified account, or go to the login page if no credentials are supplied or credentials given are incorrect.',
  inputs: {
    userName: {
      description: 'user name of the account to login to',
      type: 'string'
    },
    password: {
      description: 'password used to login',
      type: 'string'
    }
  },
  exits: {
    loginFailed: {
      description: 'Login failed because there were no credentials supplied or the credentials supplied were incorrect.',
      responseType: 'view',
      viewTemplatePath: 'pages/login'
    },
    loginSuccess: {
      description: 'Login was successful.',
      responseType: 'redirect'
    }
  },
  fn: async function(inputs, exits) {
    // load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Checks if there is a currently logged in user, and that user is a student (std = student)
    if (this.req.session.userName && this.req.session.code == 'std') {
      // Check if that user really exists in the account database. If not, failed login
      var account = await Account.findOne({user_name: this.req.session.userName});
      if (!account || account.status == 'inactive') {
        exits.loginFailed({locale: locale});
        return;
      }
      // Otherwise, login is successful, render tasks page
      exits.loginSuccess(sails.config.custom.baseUrl + 'tasks');
      return;
    }
    // If there is alogged in user, but the logged in user is a professor (fcy = faculty), delete the session
    else if (this.req.session.userName && this.req.session.code == 'fcy') {
      delete this.req.session.userName;
      delete this.req.session.code;
    }

    // If this was accessed using GET request, render the login form without logging in
    if (this.req.method == 'GET') {
      exits.loginFailed({locale: locale});
      return;
    }

    // Otherwise, check if the supplied userName and password matches a record in the account database
    var account = await Account.findOne({user_name: inputs.userName, password: inputs.password});

    // If there is no account that matches the given credentials, failed login
    if (!account || account.status == 'inactive') {
      exits.loginFailed({error: 'failed', locale: locale});
      return;
    }

    // Otherwise, set the session details and login successful
    this.req.session.userName = account.user_name;
    this.req.session.code = 'std';
    exits.loginSuccess(sails.config.custom.baseUrl + 'tasks');
    return;
  }
}
