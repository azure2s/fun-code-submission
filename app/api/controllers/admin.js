module.exports = {
  friendlyName: 'View Administrator Menu',
  description: 'Allows the admin to access administrator features.',
  inputs: {

  },
  exits: {
    adminAuthorized: {
      description: 'Account is authorized and administrator menu can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/admin'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and administrator menu cannot be viewed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    exits.adminAuthorized({locale: locale});
  }
}
