const isValidJavaIdentifier = require('valid-java-identifier');

module.exports = {
  friendlyName: 'Add Task',
  description: 'Adds a single task to the database.',
  inputs: {
    taskType: {
      description: 'The type of the task (must be either: function)',
      type: 'string'
    },
    taskId: {
      description: 'If present, edit the given task ID',
      type: 'string'
    },
    title: {
      description: 'The title of the task to be added.',
      type: 'string'
    },
    specification: {
      description: 'The task specifications. ',
      type: 'string'
    },
    /* Inputs for type code */
    defaultCode: {
      description: 'Initial code. ',
      type: 'string'
    },
    /* End of inputs for type code */
    /* Inputs for type answer */
    codeDisplay: {
      description: 'The code to display.',
      type: 'string'
    },
    questionText: {
      description: 'The list of question texts.',
      type: 'ref'
    },
    questionChoices: {
      description: 'The specification of choices for each question.',
      /* CHOICES FORMAT */
      // choice_1,choice_2,...,choice_n
      // (use "free" to indicate free-form; n must be >= 2)
      /* END OF TEST CASE FORMAT */
      type: 'ref'
    },
    questionAnswer: {
      description: 'The answers to each question.',
      type: 'ref'
    },
    /* End of inputs for type answer */
    /* Inputs for type function */
    functionName: {
      description: 'The name of the function (for function task type).',
      type: 'string'
    },
    returnType: {
      description: 'The return type of the function (for function task type).',
      type: 'string'
    },
    forVoid: {
      description: 'If the return type is void, the variable to be checked (for function task type)',
      type: 'string'
    },
    parameterTypes: {
      description: 'The datatype of the parameters of the function, separated by a comma (for function task type).',
      type: 'string'
    },
    parameterNames: {
      description: 'The name of the parameters of the function, separated by a comma (for function task type).',
      type: 'string'
    },
    testCases: {
      description: 'The test cases for the task, in CSV format (for function task type).',
      /* TEST CASE FORMAT */
      // input_1,input_2,input_3,...,input_n,output
      // input_1,input_2,input_3,...,input_n,output
      // input_1,input_2,input_3,...,input_n,output
      // input_1,input_2,input_3,...,input_n,output
      // input_1,input_2,input_3,...,input_n,output
      /* END OF TEST CASE FORMAT */
      type: 'string'
    },
    numberOfSample: {
      description: 'Number of sample test cases to display, must be less than or equal to the total number of lines of testCases.',
      type: 'number'
    }
    /* End of inputs for type function */
  },
  exits: {
    taskAdded: {
      description: 'Task was successfully added.',
      responseType: 'redirect'
    },
    taskModified: {
      description: 'Task was successfully modified',
      responseType: 'redirect'
    },
    addError: {
      description: 'An error occurred and the task was not added.',
      responseType: 'view',
      viewTemplatePath: 'pages/task/add'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    // Checks if the currently logged in user is a professor (fcy = faculty)
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    var taskValid = true;
    var errors = [];

    // If some field are missing, task is invalid
    if (inputs.taskType.trim() == '') {
      errors.push('SOME_FIELDS_MISSING');
      exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
      return;
    }
    if (inputs.title.trim() == '') {
      errors.push('SOME_FIELDS_MISSING');
      exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
      return;
    }
    if (inputs.specification.trim() == '') {
      errors.push('SOME_FIELDS_MISSING');
      exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
      return;
    }

    /* THE FOLLOWING CONDITION HANDLES THE "CODE" TYPE TASK */
    if (inputs.taskType == 'code') {
      // If no default code is provided, set it to empty string
      if (!inputs.defaultCode) {
        inputs.defaultCode = ''
      }
      // Build problem object
      var task = {
        title: inputs.title.trim(),
        type: 'code',
        specification: inputs.specification.trim(),
        default_code: inputs.defaultCode
      }

      // Build and add the task object
      try {
        if (inputs.taskId && inputs.taskId.trim() != '') {
          await Task.update({id: inputs.taskId}).set(task);
          exits.taskModified(sails.config.custom.baseUrl + 'a/task?o=e&taskId=' + inputs.taskId);
          return;
        }
        else {
          task.live = 'no';
          task.status = 'active';
          await Task.create(task);
          exits.taskAdded(sails.config.custom.baseUrl + 'a/task');
          return;
        }

      } catch(error) {
        errors = [];
        errors.push('UNKNOWN_ERROR');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
    }
    /* THE FOLLOWING HANDLES THE "ANSWER" TYPE TASK */
    else if (inputs.taskType == 'answer') {
      inputs.codeDisplay = inputs.codeDisplay.trim();

      // Check if all questions are valid
      for (var i = 0; i < inputs.questionText.length; i++) {
        var question = inputs.questionText[i].trim();
        var choices = inputs.questionChoices[i].trim();
        var answer = inputs.questionAnswer[i].trim();
        if ((question == '' && choices == '' && answer == '') || (question != '' && choices != '' && answer != '')) {

        }
        else {
          errors.push('INCOMPLETE_QUESTION');
          exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
          return;
        }
      }

      for (var i = 0; i < inputs.questionText.length; i++) {
        if (inputs.questionText[i].trim() == '') continue;
        var choiceString = inputs.questionChoices[i];
        var choices = choiceString.trim().split(',');
        if (choices.length == 1 && choiceString.trim() != 'free') {
          errors.push('NOT_ENOUGH_CHOICES');
          exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
          return;
        }
        var hasAnswer = false;
        var answer = inputs.questionAnswer[i].trim();
        if (choiceString.trim() == 'free') {
          hasAnswer = true;
        }
        else {
          for (var j = 0; j < choices.length; j++) {
            if (choices[j] == answer) {
              hasAnswer = true;
              break;
            }
          }
        }
        if (!hasAnswer) {
          errors.push('ANSWER_NOT_IN_CHOICES');
          exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
          return;
        }
        inputs.questionText[i] = inputs.questionText[i].trim();
        inputs.questionChoices[i] = inputs.questionChoices[i].trim();
        inputs.questionAnswer[i] = inputs.questionAnswer[i].trim();
      }

      // Remove non-existent questions
      for (var i = 0; i < inputs.questionText.length; i++) {
        if (inputs.questionText[i].trim() == '') {
          inputs.questionText.splice(i, 1);
          inputs.questionChoices.splice(i, 1);
          inputs.questionAnswer.splice(i, 1);
          i--;
        }
      }

      if (inputs.questionText.length == 0) {
        errors.push('NO_QUESTION_ADDED');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }

      // Build problem object
      var task = {
        title: inputs.title.trim(),
        type: 'answer',
        specification: inputs.specification.trim(),
        code_display: inputs.codeDisplay.trim(),
        question_texts: inputs.questionText,
        question_choices: inputs.questionChoices,
        question_answers: inputs.questionAnswer
      }

      try {
        if (inputs.taskId && inputs.taskId.trim() != '') {
          await Task.update({id: inputs.taskId}).set(task);
          exits.taskModified(sails.config.custom.baseUrl + 'a/task?o=e&taskId=' + inputs.taskId);
          return;
        }
        else {
          task.live = 'no';
          task.status = 'active';
          await Task.create(task);
          exits.taskAdded(sails.config.custom.baseUrl + 'a/task');
          return;
        }
      } catch(error) {
        errors = [];
        errors.push('UNKNOWN_ERROR');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
    }
    /* THE FOLLOWING HANDLES THE "FUNCTION" TYPE TASK */
    else if (inputs.taskType == 'function') {
      if (!inputs.defaultCode) {
        inputs.defaultCode = ''
      }
      if (inputs.functionName.trim() == '') {
        errors.push('SOME_FIELDS_MISSING');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
      if (inputs.returnType.trim() == '') {
        errors.push('SOME_FIELDS_MISSING');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
      if (inputs.parameterTypes.trim() == '') {
        errors.push('SOME_FIELDS_MISSING');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
      if (inputs.parameterNames.trim() == '') {
        errors.push('SOME_FIELDS_MISSING');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
      if (inputs.returnType.trim() == 'void' && inputs.forVoid.trim() == '') {
        errors.push('MUST_SPECIFY_VARIABLE_TO_CHECK_FOR_VOID_RETURN_TYPE');
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }

      var validTypes = ['int', 'float', 'double', 'boolean', 'String', 'char',
                        'int[]', 'float[]', 'double[]', 'boolean[]', 'String[]', 'char[]', 'void'];
      // Check if return type is valid
      if (validTypes.indexOf(inputs.returnType.trim()) == -1) {
        taskValid = false;
        errors.push('INVALID_RETURN_TYPE');
      }

      var parameterTypes = inputs.parameterTypes.trim().split(',');
      var parameterNames = inputs.parameterNames.trim().split(',');
      // Trim everything
      for (var i = 0; i < parameterTypes.length; i++) {
        parameterTypes[i] = parameterTypes[i].trim();
      }
      for (var i = 0; i < parameterNames.length; i++) {
        parameterNames[i] = parameterNames[i].trim();
      }

      // Check if all types are valid
      for (var i = 0; i < parameterTypes.length; i++) {
        var current = parameterTypes[i];
        if (validTypes.indexOf(current) == -1) {
          taskValid = false;
          errors.push('INVALID_DATATYPE');
          break;
        }

      }

      var paramTest = [];
      // Check if all identifiers are valid
      for (var i = 0; i < parameterNames.length; i++) {
        var current = parameterNames[i];
        if (!isValidJavaIdentifier(current)) {
          taskValid = false;
          errors.push('INVALID_IDENTIFIER');
          break;
        }
        paramTest.push(current);
      }

      if (inputs.returnType.trim() == 'void') {
        var toCheck = inputs.forVoid.trim();
        if (paramTest.indexOf(toCheck) == -1) {
          errors.push('VARIABLE_TO_CHECK_MUST_BE_ONE_OF_PARAMETERS');
          exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
          return;
        }
      }


      // CHeck if function name is valid
      if (!isValidJavaIdentifier(inputs.functionName)) {
        taskValid = false;
        errors.push('INVALID_FUNCTION_NAME');
      }

      // Check if the number of parameters match
      if (parameterTypes.length != parameterNames.length) {
        taskValid = false;
        errors.push('PARAMETER_NAME_TYPE_LENGTH_MISMATCH');
      }

      // Check test cases
      var testCaseInputs = [];
      var testCaseOutputs = [];
      var testCases = inputs.testCases.trim().split('\r\n');
      for (var i = 0; i < testCases.length; i++) {
        var current = testCases[i].match(/{[^}]*}|"[^"]*"|[^,]+/g);
        for (var j = 0; j < current.length; j++) {
          current[j] = current[j].trim();
        }
        if (current.length != parameterTypes.length + 1) {
          taskValid = false;
          errors.push('TEST_CASE_INVALID');
          break;
        }
        testCaseOutputs.push(current[current.length - 1]);
        testCaseInputs.push(current.slice(0, current.length - 1).join(','));
      }

      // Check number of test cases to show
      var numberOfSample = inputs.numberOfSample;
      if (parseInt(numberOfSample) > testCaseInputs.length) {
        taskValid = false;
        errors.push('TOO_FEW_TEST_CASES');
      }

      if (taskValid) {
        // Build problem object
        var task = {
          title: inputs.title.trim(),
          type: 'function',
          default_code: inputs.defaultCode,
          specification: inputs.specification.trim(),
          function_name: inputs.functionName.trim(),
          argument_types: parameterTypes,
          argument_names: parameterNames,
          return_type: inputs.returnType.trim(),
          sample_io_inputs: testCaseInputs,
      		sample_io_outputs: testCaseOutputs,
          sample_io_to_display: numberOfSample
        }

        if (inputs.returnType.trim() == 'void') {
          task.for_void = inputs.forVoid.trim();
        }
        else {
          task.for_void = '';
          inputs.forVoid = '';
        }

        try {
          if (inputs.taskId && inputs.taskId.trim() != '') {
            await Task.update({id: inputs.taskId}).set(task);
            exits.taskModified(sails.config.custom.baseUrl + 'a/task?o=e&taskId=' + inputs.taskId);
            return;
          }
          else {
            task.live = 'no';
            task.status = 'active';
            await Task.create(task);
            exits.taskAdded(sails.config.custom.baseUrl + 'a/task');
            return;
          }
        } catch(error) {
          errors = [];
          errors.push('UNKNOWN_ERROR');
          exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
          return;
        }
      }
      else {
        exits.addError({locale: locale, error: errors, responses: inputs, task: undefined});
        return;
      }
    }
    else {
      errors = [];
      errors.push('INVALID_TASK_TYPE');
      exits.addError({locale: locale, error: errors, responses: input, task: undefineds});
      return;
    }

    exits.taskAdded(sails.config.custom.baseUrl + 'a/task');
  }
}
