const isValidJavaIdentifier = require('valid-java-identifier');

module.exports = {
  friendlyName: 'Delete Task',
  description: 'Deletes a single task to the database.',
  inputs: {
    taskId: {
      description: 'The task ID in the database.',
      type: 'string'
    }
  },
  exits: {
    taskDeleted: {
      description: 'Task was successfully deleted.',
      statusCode: 200
    },
    deleteError: {
      description: 'An error occurred and the task was not deleted.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    if (inputs.taskId.trim() == '') {
      exits.deleteError({result: 'missing'});
      return;
    }

    try {
      await Task.update({id: inputs.taskId}).set({status: 'deleted'});
    } catch(error) {
      exits.deleteError({result: 'failed'})
      return;
    }

    exits.taskDeleted({result: 'success'});
  }
}
