const isValidJavaIdentifier = require('valid-java-identifier');

module.exports = {
  friendlyName: 'Toggle Task',
  description: 'If live, changes a task to go not live and vice versa.',
  inputs: {
    taskId: {
      description: 'The task ID in the database.',
      type: 'string'
    }
  },
  exits: {
    taskToggled: {
      description: 'Task was successfully toggled.',
      statusCode: 200
    },
    toggleError: {
      description: 'An error occurred and the task was not toggled.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and accounts cannot be managed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    if (inputs.taskId.trim() == '') {
      exits.toggleError({result: 'missing'});
      return;
    }

    try {
      var task = await Task.findOne({id: inputs.taskId});
      if (!task) {
        exits.toggleError({result: 'invalid'})
        return;
      }
      var target = task.live == 'no' ? 'yes' : 'no';
      await Task.update({id: inputs.taskId}).set({live: target});
    } catch(error) {
      exits.toggleError({result: 'failed'})
      return;
    }

    exits.taskToggled({result: 'success'});
  }
}
