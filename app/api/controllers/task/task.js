module.exports = {
  friendlyName: 'Manage Tasks',
  description: 'Allows the admin to manage tasks.',
  inputs: {
    o: {
      description: 'option on what action should be made (v=view, a=add, e=examine, m=modify)',
      type: 'string'
    },
    taskId: {
      description: 'the task ID to be examined, only relevant if o=e',
      type: 'string'
    }
  },
  exits: {
    adminAuthorizedView: {
      description: 'Account is authorized and tasks can be managed.',
      responseType: 'view',
      viewTemplatePath: 'pages/task/task'
    },
    adminAuthorizedAdd: {
      description: 'Account is authorized and a task can be added.',
      responseType: 'view',
      viewTemplatePath: 'pages/task/add'
    },
    adminAuthorizedExamine:{
      description: 'Account is authorized and the task can be examined.',
      responseType: 'view',
      viewTemplatePath: 'pages/task/examine'
    },
    adminAuthorizedModify: {
      description: 'Account is authorized and task can be modified.',
      responseType: 'view',
      viewTemplatePath: 'pages/task/add'
    },
    adminUnauthorized: {
      description: 'Account is not authorized and tasks cannot be managed.',
      responseType: 'notFound'
    },
    invalidTaskId: {
      description: 'Invalid task ID was provided.',
      responseType: 'notFound'
    }
  },
  fn: async function(inputs, exits) {
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized();
      return;
    }
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized();
      return;
    }

    if (!inputs.o) {
      inputs.o = 'v';
    }

    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});
    if (inputs.o == 'a') {
      exits.adminAuthorizedAdd({locale: locale, task: undefined, responses: undefined});
      return;
    }
    else if (inputs.o == 'e') {
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidTaskId();
        return;
      }
      var task = await Task.findOne({id: inputs.taskId, status: 'active'});
      if (!task) {
        exits.invalidTaskId();
        return;
      }
      task = await sails.helpers.preprocessTask.with({task: task});
      exits.adminAuthorizedExamine({locale: locale, task: task});
      return;
    }
    else if (inputs.o == 'm') {
      if (!inputs.taskId || inputs.taskId.trim() == '') {
        exits.invalidTaskId();
        return;
      }
      var task = await Task.findOne({id: inputs.taskId, status: 'active'});
      if (!task) {
        exits.invalidTaskId();
        return;
      }
      var testCases = '';
      if (task.sample_io_inputs && task.sample_io_inputs != null) {
        for (var i = 0; i < task.sample_io_inputs.length; i++) {
          testCases += task.sample_io_inputs[i] + ',' + task.sample_io_outputs[i] + '\n';
        }
      }
      var responses = {
        taskType: task.type ? task.type : undefined,
        taskId: task.id ? task.id : undefined,
        title: task.title ? task.title : undefined,
        specification: task.specification ? task.specification : undefined,
        codeDisplay: task.code_display ? task.code_display : undefined,
        defaultCode: task.default_code ? task.default_code : undefined,
        questionText: task.question_texts ? task.question_texts : undefined,
        questionChoices: task.question_choices ? task.question_choices : undefined,
        questionChoices: task.question_choices ? task.question_choices : undefined,
        questionAnswer: task.question_answers ? task.question_answers : undefined,
        functionName: task.function_name ? task.function_name : undefined,
        returnType: task.return_type ? task.return_type : undefined,
        forVoid: task.for_void ? task.for_void : undefined,
        parameterTypes: task.argument_types ? task.argument_types : undefined,
        parameterNames: task.argument_names ? task.argument_names : undefined,
        testCases: testCases ? testCases : undefined,
        numberOfSample: task.sample_io_to_display ? parseInt(task.sample_io_to_display) : undefined
      }
      exits.adminAuthorizedModify({locale: locale, task: task, responses: responses});
      return;
    }

    // Check if past submissions are visible or note
    var pastVisible = false;
    var vSetting = await Setting.findOne({settingkey: 'past'});
    if (!vSetting) {
      pastVisible = false;
    }
    else if (vSetting.settingvalue == 'true') {
      pastVisible = true;
    }
    else {
      pastVisible = false;
    }

    var tasks = await Task.find({select: ['id', 'title', 'live', 'createdAt'], where: {status: 'active'}});
    exits.adminAuthorizedView({locale: locale, pastVisible: pastVisible, tasks: tasks});
  }
}
