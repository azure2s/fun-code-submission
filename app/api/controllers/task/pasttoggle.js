module.exports = {
  friendlyName: 'Toggle Past Submissions Visibility',
  description: 'Toggles the visibility of past submissions',
  inputs: {

  },
  exits: {
    visibilityToggled: {
      description: 'Visibility was successfully toggled.',
      statusCode: 200
    },
    visibilityToggledError: {
      description: 'An error occurred and the visibility was not toggled.',
      statusCode: 200
    },
    adminUnauthorized: {
      description: 'Account is not authorized and settings cannot be changed.',
      statusCode: 200
    }
  },
  fn: async function(inputs, exits) {
    if (!this.req.session.userName || !this.req.session.code) {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    else if (this.req.session.code != 'fcy') {
      exits.adminUnauthorized({result: 'unauthorized'});
      return;
    }
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    var vSetting = await Setting.findOne({settingkey: 'past'});
    if (!vSetting) {
      exits.visibilityToggledError({result: 'error'});
      return;
    }

    if (vSetting.settingvalue == 'true') {
      await Setting.update({settingkey: 'past'}).set({settingvalue: 'false'});
    }
    else {
      await Setting.update({settingkey: 'past'}).set({settingvalue: 'true'});
    }

    exits.visibilityToggled({result: 'success'});
  }
}
