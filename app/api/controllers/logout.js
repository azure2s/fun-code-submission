module.exports = {
  friendlyName: 'Logout',
  description: 'Logout the currently active account, if present.',
  exits: {
    success: {
      description: 'Session was successfully destroyed.',
      responseType: 'redirect'
    }
  },
  fn: function(inputs, exits) {
    // Check if the currently logged in user is a student or professor
    var type = 's';
    if (this.req.session.code) {
      if (this.req.session.code == 'fcy') {
        type = 'f';
      }
    }

    // Logout by deleting session
    delete this.req.session.userName;
    delete this.req.session.code;

    // If student, redirect to student login, and if professor, redirect to faculty login
    if (type == 's') {
      exits.success('/login');
      return;
    }
    exits.success('/flgn');
  }
}
