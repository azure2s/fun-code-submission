module.exports = {
  friendlyName: 'Solve Problem',
  description: 'Allow the student to solve the problem.',
  inputs: {
    id: {
      description: 'The problem ID',
      type: 'string'
    }
  },
  exits: {
    accountAuthorized: {
      description: 'User can proceed to solve the problem.',
      responseType: 'view',
      viewTemplatePath: 'pages/solve'
    },
    invalidProblemID: {
      description: 'The problem ID supplied does not exist.',
      responseType: 'notFound'
    },
    problemIsNotAvailable: {
      description: 'Solving the problem is currently restricted.',
      responseType: 'notFound'
    },
    accountUnauthorized: {
      description: 'Account is not authorized and problem cannot be solved.',
      responseType: 'redirect'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.accountUnauthorized('login');
      return;
    }
    // Checks if the currently logged in user is a student (std = student)
    else if (this.req.session.code != 'std') {
      exits.accountUnauthorized('login');
      return;
    }

    // If no ID is provided, respond with error
    if (!inputs.id) {
      exits.invalidProblemID();
    }

    // Check if the currently logged in user is still active in the database
    var currentUser = await Account.findOne({where: {'user_name': this.req.session.userName}});
    if (!currentUser || currentUser.status == 'inactive') {
      exits.accountUnauthorized('login');
      return;
    }
    var currentUserId = currentUser.id;

    // If the task does not exist in the database, respond with error
    var task = await Task.findOne({id: inputs.id});
    if (!task) {
      exits.invalidProblemID();
      return;
    }

    // If the task is not live (i.e., not accepting submissions, respond with error_
    if (task.live == 'no') {
      exits.invalidProblemID();
      return;
    }

    // Preprocess the task to prepare it for displaying in HTML
    task = await sails.helpers.preprocessTask.with({task: task});

    // Get the submission fo the currently logged in user to the given task, if it already exists.
    var submission = await Submission.findOne({where: {user: currentUserId, task: task.id}});
    if (submission) {
      // If that submission exists, preprocess the submission to prepare it for displaying in HTML
      submission = await sails.helpers.preprocessSubmission.with({submission: submission});
    }

    // Load the texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Render the solve page
    exits.accountAuthorized({locale: locale, task: task, submission: submission, user_name: this.req.session.userName});
  }
}
