module.exports = {
  friendlyName: 'View Tasks',
  description: 'View the currently available tasks.',
  inputs: {
  },
  exits: {
    accountAuthorized: {
      description: 'Account is authorized and tasks can be viewed.',
      responseType: 'view',
      viewTemplatePath: 'pages/tasks'
    },
    accountUnauthorized: {
      description: 'Account is not authorized and tasks cannot be viewed.',
      responseType: 'redirect'
    }
  },
  fn: async function(inputs, exits) {
    // Checks if there is a currently logged in user
    if (!this.req.session.userName || !this.req.session.code) {
      exits.accountUnauthorized('login');
      return;
    }
    // Checks if the currently logged in user is a student (std = student)
    else if (this.req.session.code != 'std') {
      exits.accountUnauthorized('login');
      return;
    }

    // Get all active and live tasks in the database
    var tasks = await Task.find({select: ['id', 'title'], where: {status: 'active', live: 'yes'}});

    // Check if the currently logged in user is still active in the database
    var currentUser = await Account.findOne({where: {'user_name': this.req.session.userName}});
    if (!currentUser || currentUser.status == 'inactive') {
      exits.accountUnauthorized('login');
      return;
    }

    // Get all submissions of the current user
    var currentUserId = currentUser.id;
    var submissions = await Submission.find({where: {user: currentUserId}}).populate('task');
    // Get all the IDs of the tasks in which the current user has already sbumitted to
    var submittedTasks = submissions.map(x => x.task.id);

    // Load texts
    var locale = await sails.helpers.loadLocale.with({language: sails.config.custom.language});

    // Check if past submissions are visible or note
    var pastVisible = false;
    var vSetting = await Setting.findOne({settingkey: 'past'});
    if (!vSetting) {
      pastVisible = false;
    }
    else if (vSetting.settingvalue == 'true') {
      pastVisible = true;
    }
    else {
      pastVisible = false;
    }

    // Render task list page
    exits.accountAuthorized({locale: locale, pastVisible: pastVisible, tasks: tasks, submissions: submissions, submittedTasks: submittedTasks, user_name: this.req.session.userName});
  }
}
