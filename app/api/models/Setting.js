module.exports = {
  attributes: {
    settingkey: {type: 'string', required: true},
    settingvalue: {type: 'string', required: true},
  }
}
