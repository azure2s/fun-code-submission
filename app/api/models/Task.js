module.exports = {
  attributes: {
    title: {type: 'string', required: true },
    type: { type: 'string', required: true },
    specification: {type: 'string', required: true },
    default_code: {type: 'string'},
    function_name: {type: 'string' },
    argument_types: {type: 'json', columnType: 'array' },
    argument_names: {type: 'json', columnType: 'array' },
    return_type: {type: 'string' },
    for_void: {type: 'string'},
    sample_io_inputs: { type: 'json', columnType: 'array' },
		sample_io_outputs: { type: 'json', columnType: 'array' },
    sample_io_to_display: { type: 'number' },
    code_display: { type: 'string' },
    question_texts: { type: 'json', columnType: 'array' },
    question_choices: { type: 'json', columnType: 'array' },
    question_answers: { type: 'json', columnType: 'array' },
    status: { type: 'string', required: true },
    live: { type: 'string', required: true }
  }
}
