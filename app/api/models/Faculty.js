module.exports = {
  attributes: {
    user_name: {type: 'string', required: true},
    password: {type: 'string', required: true},
    name: {type: 'string', required: true}
  }
}
