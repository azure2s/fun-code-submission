module.exports = {
  attributes: {
    user: {model: 'account', required: true},
    task: {model: 'task', required: true},
    response: {type: 'json', columnType: 'array'}
  }
}
