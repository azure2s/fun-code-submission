data = {
  login_header: {
    en: 'Login Page',
    jp: 'ログインする'
  },
  faculty_login_header: {
    en: 'Admin',
    jp: '管理者'
  },
  student_redirect: {
    en: 'For students, please click here.',
    jp: '学生の方はこちらをクリックしてください。'
  },
  time_created: {
    en: 'Time Created',
    jp: '作成時間'
  },
  user_name: {
    en: 'User Name',
    jp: 'ユーザー名'
  },
  password: {
    en: 'Password',
    jp: 'パスワード'
  },
  show_password: {
    en: 'Show Password',
    jp: 'パスワードを表示する'
  },
  login: {
    en: 'Log in',
    jp: 'ログイン'
  },
  lost_password: {
    en: 'If forgot your password, please send an email to kaoru-lab@fun.ac.jp.',
    jp: 'パスワードを紛失した場合は、kaoru-lab@fun.ac.jpまでメールしてください。'
  },
  invalid_login: {
    en: 'Invalid login. Please try again.',
    jp: '入力されたメールアドレスとパスワードのアカウントは存在しません。もう一度お試しください。'
  },
  tasks_header: {
    en: 'Problem List',
    jp: '問題リスト'
  },
  past_tasks_header: {
    en: 'Previous Submissions',
    jp: '以前の提出物'
  },
  task_id: {
    en: 'ID',
    jp: 'ID'
  },
  task_title: {
    en: 'Name',
    jp: '問題のタイトル'
  },
  task_live: {
    en: 'Accepting Submissions?',
    jp: '提出受入中?'
  },
  task_delete: {
    en: 'Delete',
    jp: '削除'
  },
  task_toggle_live: {
    en: 'Toggle',
    jp: '切替'
  },
  task_delete_confirm: {
    en: 'Are you sure? This action cannot be undone.',
    jp: '本当によろしいですか？この操作は元に戻せません。'
  },
  answer: {
    en: 'Answer',
    jp: '提出する'
  },
  task_submitted: {
    en: 'Submitted',
    jp: '提出した？'
  },
  log_out_topbar: {
    en: 'Log out',
    jp: 'ログアウト'
  },
  back_to_tasks: {
    en: 'Back to Problem List',
    jp: '問題リストに戻る'
  },
  sample_io: {
    en: 'Sample Input / Output:',
    jp: '入力／出力例'
  },
  output: {
    en: 'Output',
    jp: '出力'
  },
  after_calling: {
    en: 'After calling the function:',
    jp: '関数を呼び出した後:'
  },
  submit_code: {
    en: 'Submit',
    jp: '提出する'
  },
  resubmit_code: {
    en: 'Resubmit',
    jp: '再提出する'
  },
  submit_confirm: {
    en: 'Are you sure you want to submit this code? This will overwrite previous submission.',
    jp: 'このコードを送信してもよろしいですか？これにより、以前の送信が上書きされます。'
  },
  submit_yes: {
    en: 'Yes',
    jp: 'はい'
  },
  submit_no: {
    en: 'No',
    jp: 'いいえ'
  },
  already_submit: {
    en: 'You already submitted an asnwer for this problem.',
    jp: 'この問題に対する回答をすでに提出しています。'
  },
  submission_successful_header: {
    en: 'Submission Successful',
    jp: '提出に成功しました'
  },
  problem_not_available_header: {
    en: 'Error: Problem Not Available',
    jp: 'エラー：利用できない問題'
  },
  submission_already_exists_header: {
    en: 'Error: Submission Already Exists',
    jp: 'エラー：提出は既に存在します'
  },
  submission_error_header: {
    en: 'Error: Submission Failed',
    jp: 'エラー：送信に失敗しました'
  },
  view_submission: {
    en: 'View Submission',
    jp: '提出物を見る'
  },
  problem_not_available_text: {
    en: 'This may be because the problem ID is invalid or because the problem is not accepting submissions at this time.',
    jp: 'これは、問題IDが無効であるか、問題が現時点で提出を受け付けていないことが原因である可能性があります。'
  },
  submission_already_exists_text: {
    en: 'You have already submitted for this problem before so you cannot submit again.',
    jp: 'この問題についてはすでに提出済みであるため、再度提出することはできません。'
  },
  submission_error_text: {
    en: 'An unknown error occurred in the submission process. Please try again.',
    jp: '送信プロセスで不明なエラーが発生しました。もう一度やり直してください。'
  },
  back: {
    en: 'Back',
    jp: '戻る'
  },
  accounts_topbar: {
    en: 'Accounts',
    jp: 'アカウントリスト'
  },
  account_header: {
    en: 'List of Accounts',
    jp: 'アカウントのリスト'
  },
  account_name: {
    en: 'Name',
    jp: '名前'
  },
  account_user_name: {
    en: 'User Name',
    jp: 'ユーザー名'
  },
  account_password: {
    en: 'Password',
    jp: 'パスワード'
  },
  account_active: {
    en: 'Active?',
    jp: 'アクティブ？'
  },
  account_deactivate: {
    en: 'Deactivate',
    jp: '無効化'
  },
  account_delete: {
    en: 'Delete',
    jp: '削除する'
  },
  show_active_only: {
    en: 'Show Active Only',
    jp: 'アクティブのみを表示'
  },
  show_inactive_only: {
    en: 'Show Inactive Only',
    jp: '非アクティブのみを表示'
  },
  search: {
    en: 'Search',
    jp: '探す'
  },
  view: {
    en: 'View',
    jp: '見る'
  },
  edit: {
    en: 'Edit',
    jp: '修正'
  },
  show_all: {
    en: 'Show All',
    jp: 'すべて表示する'
  },
  account_deactivate_confirm: {
    en: 'Are you sure? This action cannot be reversed.',
    jp: '本当によろしいですか？このアクションを元に戻すことはできません。'
  },
  account_delete_confirm: {
    en: 'WARNING: If you delete an account, all submissions of that account will also be deleted permanently. Please enter the account user name to confirm.',
    jp: '警告：アカウントを削除すると、そのアカウントのすべての送信も完全に削除されます。確認のため、アカウントのユーザー名を入力してください。'
  },
  account_add: {
    en: 'Add Account',
    jp: 'アカウントを追加する'
  },
  account_add_button: {
    en: 'Add',
    jp: '追加する'
  },
  account_batch_add_button: {
    en: 'Batch Add',
    jp: '一括追加する'
  },
  account_add_error: {
    en: 'Error adding the account.',
    jp: 'アカウントの追加中にエラーが発生しました。'
  },
  account_add_error_missing: {
    en: 'Some details are missing.',
    jp: '一部の詳細が欠落しています。'
  },
  account_add_error_duplicate: {
    en: 'There is already an account with this user name:',
    jp: 'このユーザー名のアカウントは既に存在します：'
  },
  account_add_error_double: {
    en: 'This user name appears multiple times in the list:',
    jp: 'このユーザー名はリストに複数回表示されます：'
  },
  account_add_error_format: {
    en: 'There is an error on this line:',
    jp: 'この行にエラーがあります：'
  },
  account_batch_specs: {
    en: 'Account List',
    jp: 'アカウントリスト'
  },
  account_batch_reminders: {
    en: 'Please be careful to follow the format when adding accounts. It is recommended to first write the accounts on a separate text file before copying it to the text area.',
    jp: 'アカウントを追加するときは、フォーマットに注意してください。テキスト領域にコピーする前に、まず別のテキストファイルにアカウントを書き込むことをお勧めします。'
  },
  tasks_topbar: {
    en: 'Problems',
    jp: '問題リスト'
  },
  task_header: {
    en: 'Problem List',
    jp: '問題リスト'
  },
  task_add: {
    en: 'Add Problem',
    jp: '問題を追加'
  },
  task_edit: {
    en: 'Update Problem',
    jp: '問題を更新する'
  },
  task_add_button: {
    en: 'Add',
    jp: '追加する'
  },
  task_specification: {
    en: 'Specifications',
    jp: '問題テキスト'
  },
  task_default_code: {
    en: 'Initial Code (optional)',
    jp: '初期コード（オプション）'
  },
  task_code: {
    en: 'Code to display (optional)',
    jp: '表示するコード（オプション）'
  },
  task_type: {
    en: 'Problem Type',
    jp: '問題のタイプ'
  },
  task_type_function: {
    en: 'Complete the function (Category 4/5)',
    jp: '関数を完了する (カテゴリー4/5)'
  },
  task_type_answer: {
    en: 'Answer the question (Category 2/3)',
    jp: '質問に答える(カテゴリー2/3)'
  },
  task_type_code: {
    en: 'Write some code (Category 1)',
    jp: 'いくつかのコードを書く (カテゴリ1)'
  },
  function_name: {
    en: 'Function Name',
    jp: '関数名'
  },
  return_type: {
    en: 'Return Type',
    jp: '戻りタイプ'
  },
  parameter_names: {
    en: 'Parameter Names',
    jp: 'パラメータ名'
  },
  parameter_types: {
    en: 'Parameter Types',
    jp: 'パラメータのタイプ'
  },
  for_void: {
    en: 'If void, the variable to check',
    jp: '「void」の場合、チェックする変数'
  },
  test_cases: {
    en: 'Test Cases',
    jp: 'テストケース'
  },
  show_top_n_cases_as_sample_io: {
    en: 'Show n cases as sample input/output',
    jp: '入力／出力例としてn個のケースを表示'
  },
  question_text: {
    en: 'Question Text (Leave blank if unused)',
    jp: '質問テキスト（未使用の場合は空白のままにします）'
  },
  question_choices: {
    en: 'Question Choices (Type "free" for free-form answer)',
    jp: '質問の選択肢 (自由形式の回答の場合は free と入力します）'
  },
  question_answer: {
    en: 'Question Answer',
    jp: '質疑応答'
  },
  task_add_error_some_fields_missing: {
    en: 'ERROR: Some fields are missing.',
    jp: 'エラー：一部のフィールドが欠落しています。'
  },
  task_add_error_invalid_return_type: {
    en: 'ERROR: Invalid return type. Must be int, float, double, String, char, int[], float[], double[], String[], char[], void',
    jp: 'エラー：無効な戻りタイプです。int, float, double, String, char, int [], float [], double [], String [], char [], voidである必要があります'
  },
  task_add_error_invalid_datatype: {
    en: 'ERROR: Some paramater types are invalid. Must be int, float, double, String, char',
    jp: 'エラー：一部のパラメータータイプが無効です。 int、float、double、String、charである必要があります。'
  },
  task_add_error_invalid_identifier: {
    en: 'ERROR: Some parameter names are invalid.',
    jp: 'エラー：一部のパラメータ名が無効です。'
  },
  task_add_error_invalid_function_name: {
    en: 'ERROR: The function name is invalid.',
    jp: 'エラー：関数名が無効です。'
  },
  task_add_error_parameter_name_type_length_mismatch: {
    en: 'ERROR: The number of paramater types do not match the number of parameter names.',
    jp: 'エラー：パラメータタイプの数がパラメータ名の数と一致しません。'
  },
  task_add_error_test_case_invalid: {
    en: 'ERROR: Make sure the test cases are in the correct format.',
    jp: 'エラー：テストケースの形式が正しいことを確認してください。'
  },
  task_add_error_too_few_test_cases: {
    en: 'ERROR: The number of test cases to display is more than the number of test cases.',
    jp: 'エラー：表示するテストケースの数は、テストケースの数を超えています。'
  },
  task_add_error_unknown_error: {
    en: 'ERROR: An error occurred in adding the problem.',
    jp: 'エラー：問題の追加中にエラーが発生しました。'
  },
  task_add_error_invalid_task_type: {
    en: 'ERROR: Invalid problem type.',
    jp: 'エラー：無効な問題タイプ。'
  },
  submission_task_list_header: {
    en: 'View Submissions',
    jp: '提出物を見る'
  },
  submission_header: {
    en: 'Submissions',
    jp: '提出物'
  },
  submission_user_name: {
    en: 'User Name',
    jp: 'ユーザー名'
  },
  submission_name: {
    en: 'Name',
    jp: '名前'
  },
  submission_id: {
    en: 'ID',
    jp: 'ID'
  },
  submitted_by: {
    en: 'Submitted by:',
    jp: '提出者:'
  },
  submission_time: {
    en: 'Time',
    jp: '提出時間'
  },
  submission_count: {
    en: 'Submission Count',
    jp: '提出数'
  },
  download_all: {
    en: 'Download Submissions for All Problems',
    jp: 'ダウンロードJSON'
  },
  download: {
    en: 'Download',
    jp: 'ダウンロード'
  },
  custom_download: {
    en: 'Custom Download',
    jp: 'カスタムダウンロード'
  },
  download_for_problem: {
    en: 'Download Submissions for this Problem',
    jp: 'ダウンロードJSON'
  },
  download_for_problem_checked: {
    en: 'Download Submissions for this Problem (checked)',
    jp: 'ダウンロードチェックJSON'
  },
  download_for_user: {
    en: 'Download Submissions for this Student',
    jp: 'ダウンロードJSON'
  },
  download_for_user_checked: {
    en: 'Download Submissions for this Student (checked)',
    jp: 'ダウンロードチェックJSON'
  },
  download_for_selected_users: {
    en: 'Download for Selected Users',
    jp: 'ユーザーを選択'
  },
  download_checked: {
    en: 'Download (checked)',
    jp: 'ダウンロードチェックJSON'
  },
  download_excel_raw: {
    en: 'Raw Submissions Excel Download',
    jp: 'ダウンロードExcel'
  },
  checking_wait: {
    en: 'Checking. Please wait...',
    jp: 'チェックしています。お待ちください...'
  },
  professor: {
    en: 'Professor Account',
    jp: '管理者アカウント'
  },
  admin_menu: {
    en: 'Menu',
    jp: 'メニュー'
  },
  manage_accounts: {
    en: 'Manage Accounts',
    jp: 'アカウントを管理'
  },
  manage_tasks: {
    en: 'Manage Problems',
    jp: '問題を管理'
  },
  manage_submissions: {
    en: 'Manage Submissions',
    jp: '提出物を管理'
  },
  actions: {
    en: 'Actions',
    jp: 'オペレーション'
  },
  error_occurred: {
    en: 'Operation failed.',
    jp: '操作に失敗しました。'
  },
  add_to_list: {
    en: 'Add to List',
    jp: 'リストに追加する'
  },
  active_inactive: {
    en: 'Active / Inactive',
    jp: 'アクティブ/非アクティブ'
  },
  select_user: {
    en: 'Input user names to download:',
    jp: 'ダウンロードするユーザー名を入力してください：'
  },
  toggle_past_visible: {
    en: 'Change',
    jp: '変更'
  },
  past_visible: {
    en: 'Students can view past submissions',
    jp: '過去の提出物を閲覧できます'
  },
  past_not_visible: {
    en: 'Students cannot view past submissions',
    jp: '過去の提出物を見ることができません'
  }
}

module.exports = {data};
