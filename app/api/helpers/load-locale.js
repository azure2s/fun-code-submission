const locale = require('../app_modules/locale.js');

module.exports = {
  friendlyName: 'Load Locale',
  description: 'This loads the selected language for the application.',
  inputs: {
    language: {
      type: 'string',
      description: 'either \'en\' or \'jp\', represents the language to be extracted',
      required: true
    }
  },
  fn: async function(inputs, exits) {
    var data = locale.data;
    var res = {};
    for (var text in data) {
      if (!data.hasOwnProperty(text)) continue;
      if (data[text][inputs.language]) {
        res[text]  = data[text][inputs.language];
      }
      else {
        // If no corresponding text was found for the chosen language, "NO TEXT FOUND" is assigned.
        // If you encounter this, consider editing ../app_modules/locale.js to add the text for the chosen language.
        res[text] = 'NO TEXT FOUND';
      }
    }
    return exits.success(res);
  }
}
