const replaceall = require('replaceall')
const jsStringEscape = require('js-string-escape')

module.exports = {
  friendlyName: 'Preprocess Task',
  description: 'This performs some pre-processing on the task object before it is displayed to the user.',
  inputs: {
    task: {
      type: 'json',
      description: 'the task object',
      required: true
    }
  },
  fn: async function(inputs, exits) {
    var specification = inputs.task.specification;
    specification = replaceall('<', '＜', specification);
    specification = replaceall('>', '＞', specification);
    specification = replaceall('``', '</span>', specification);
    specification = replaceall('`', '<span class="code">', specification);
    specification = replaceall('[/img]', '" class="specification-image" />' , specification);
    specification = replaceall('[img]', '<img src="' , specification);
    specification = replaceall('\n', '<br />' , specification);
    inputs.task.specification = specification;

    if (!inputs.defaultCode) {
      inputs.defaultCode = ''
    }
    inputs.defaultCode = jsStringEscape(inputs.defaultCode);

    if (inputs.task.sample_io_inputs) {
      for (var i = 0; i < inputs.task.sample_io_inputs.length; i++) {
        var current = inputs.task.sample_io_inputs[i];
        current = current.match(/{[^}]*}|"[^"]*"|[^,]+/g);
        inputs.task.sample_io_inputs[i] = current;
      }
    }
    if (inputs.task.question_choices) {
      for (var i = 0; i < inputs.task.question_choices.length; i++) {
        var current = inputs.task.question_choices[i];
        if (current != 'free') {
          current = current.split(",");
          inputs.task.question_choices[i] = current;
        }
      }
    }
    if (inputs.task.question_texts) {
      for (var i = 0; i < inputs.task.question_texts.length; i++) {
        inputs.task.question_texts[i] = replaceall('<', '<', inputs.task.question_texts[i]);
        inputs.task.question_texts[i] = replaceall('>', '>', inputs.task.question_texts[i]);
        inputs.task.question_texts[i] = replaceall('``', '</span>', inputs.task.question_texts[i]);
        inputs.task.question_texts[i] = replaceall('`', '<span class="code">', inputs.task.question_texts[i]);
      }
    }
    return exits.success(inputs.task);
  }
}
