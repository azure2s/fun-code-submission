const { java } = require('compile-run')

module.exports = {
  friendlyName: 'Run Code',
  description: 'Runs the given code',
  inputs: {
    code: {
      type: 'string',
      description: 'the code to be run',
      required: true
    }
  },
  fn: async function(inputs, exits) {
    let resultPromise = java.runSource(inputs.code);
    resultPromise.then(result => {
      return exits.success({type: 'success', source: inputs.code, result: result});
    }).catch(e => {
      return exits.success({type: 'error', source: inputs.code, result: result});
    });

  }
}
