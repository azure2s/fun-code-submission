const jsStringEscape = require('js-string-escape')

module.exports = {
  friendlyName: 'Preprocess Submission',
  description: 'This performs some pre-processing on the submission object before it is displayed to the user.',
  inputs: {
    submission: {
      type: 'json',
      description: 'the submission object',
      required: true
    }
  },
  fn: async function(inputs, exits) {
    for (var i = 0; i < inputs.submission.response.length; i++) {
      inputs.submission.response[i] = jsStringEscape(inputs.submission.response[i]);
    }
    return exits.success(inputs.submission);
  }
}
