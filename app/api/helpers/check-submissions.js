const fs = require('fs');
const replaceall = require('replaceall');

const IMPORT_STATEMENT = 'import java.io.*;\n';
const CLASS_HEADER = 'public class Main {\n';
const CLOSE_BRACKET = '}\n';
const MAIN_METHOD_HEADER = 'public static void main(String[] args) {\n';
const OBJECT_INSTIATION = 'Main o = new Main();\n'
const PRINTSTREAM_INITIALIZATION = 'PrintStream _originalStream = System.out;\n' +
  'PrintStream _dummyStream = new PrintStream(new OutputStream() { public void write(int b) {} });\n' +
  'System.setOut(_dummyStream);\n';
const RESULT_STRING = 'String _result = "";\n';
const OUTPUT_STRING = 'System.setOut(_originalStream);\n' +
  'System.out.print(_result);';

module.exports = {
  friendlyName: 'Check Submissions',
  description: 'This performs automatic checking on the submissions on the server side.',
  inputs: {
    submissions: {
      type: 'json',
      description: 'the submissions object',
      required: true
    }
  },
  fn: async function(inputs, exits) {
    var submissions = inputs.submissions;
    var submissionsLength = submissions.length;
    var checkedSubmissions = [];

    async function wrapCode(code, task) {
      var res = IMPORT_STATEMENT;
      res += CLASS_HEADER;

      res += task.return_type + ' ' + task.function_name + '(';
      for (var i = 0; i < task.argument_names.length; i++) {
        res += task.argument_types[i] + ' ' + task.argument_names[i];
        if (i != task.argument_names.length - 1) {
          res += ', ';
        }
      }
      res += ') {\n';

      res += code + '\n';
      res += CLOSE_BRACKET;
      res += MAIN_METHOD_HEADER;
      res += OBJECT_INSTIATION;
      res += PRINTSTREAM_INITIALIZATION;
      res += RESULT_STRING;

      if (task.return_type.charAt(task.return_type.length - 1) == ']') {
        res += task.return_type + ' _temp;\n';
      }

      for (var i = 0; i < task.argument_names.length; i++) {
        res += task.argument_types[i] + ' ' + task.argument_names[i] + ';\n'
      }

      for (var i = 0; i < task.sample_io_inputs.length; i++) {
        task.sample_io_inputs[i] =   task.sample_io_inputs[i].match(/{[^}]*}|"[^"]*"|[^,]+/g);
      }

      for (var i = 0; i < task.sample_io_inputs.length; i++) {
        for (var j = 0; j < task.argument_names.length; j++) {
          if (task.argument_types[j].charAt(task.argument_types[j].length - 1) != ']')
            res += task.argument_names[j] + ' = ' + task.sample_io_inputs[i][j] + ';\n';
          else
            res += task.argument_names[j] + ' = new ' + task.argument_types[j] + task.sample_io_inputs[i][j] + ';\n';
        }
        if (task.return_type.charAt(task.return_type.length - 1) != ']') {
          if (task.return_type != 'void') {
            res += '_result += o.' + task.function_name + '(';
            for (var j = 0; j < task.argument_names.length; j++) {
              res += task.argument_names[j];
              if (j != task.argument_names.length - 1) {
                res += ', ';
              }
            }
            res += ') + "\\n";\n';
          }
          else {
            res += 'o.' + task.function_name + '(';
            for (var j = 0; j < task.argument_names.length; j++) {
              res += task.argument_names[j];
              if (j != task.argument_names.length - 1) {
                res += ', ';
              }
            }
            res += ');\n';
          }
        }
        else {
          if (task.return_tyoe != 'void') {
            res += '_temp = o.' + task.function_name + '(';
            for (var j = 0; j < task.argument_names.length; j++) {
              res += task.argument_names[j];
              if (j != task.argument_names.length - 1) {
                res += ', ';
              }
            }
            res += ');\n';

            res += '_result += "{";\n';
            res += 'for (int i = 0; i < _temp.length; i++) {\n';
            res += '_result += _temp[i];\n'
            res += 'if (i != _temp.length - 1) _result += ",";\n';
            res += '}\n'
            res += '_result += "}\\n";\n';
          }
          else {
            res += 'o.' + task.function_name + '(';
            for (var j = 0; j < task.argument_names.length; j++) {
              res += task.argument_names[j];
              if (j != task.argument_names.length - 1) {
                res += ', ';
              }
            }
            res += ');\n';
          }
        }

        if (task.return_type == 'void') {
          var toCheck = task.for_void;
          var forVoidType = task.argument_types[task.argument_names.indexOf(toCheck)];
          if (forVoidType.charAt(forVoidType.length - 1) == ']') {
            res += '_result += "{";\n';
            res += 'for (int i = 0; i < ' + task.for_void + '.length; i++) {\n';
            res += '_result += ' + task.for_void + '[i];\n'
            res += 'if (i != ' + task.for_void + '.length - 1) _result += ",";\n';
            res += '}\n'
            res += '_result += "}\\n";\n';
          }
          else {
            res += '_result += ' + task.for_void + '"}\\n";\n';
          }
        }
      }

      res += OUTPUT_STRING;
      res += CLOSE_BRACKET;
      res += CLOSE_BRACKET;

      // For Processing
      res = replaceall('float', 'double', res);

      for (var i = 0; i < task.sample_io_inputs.length; i++) {
        task.sample_io_inputs[i] =   task.sample_io_inputs[i].join(',');
      }

      return res;
    }

    while (submissions.length != 0) {
      var submission = submissions.splice(0,1)[0];
      var targetTask = submission.task;
      var checker = {};
      if (targetTask.type == 'answer') {
        var verdict  = 'none'
        var details = '';
        var submissionResponses = submission.response;
        var expectedResponses = targetTask.question_answers;
        if (submissionResponses.length != expectedResponses.length) {
          verdict = 'wrong';
          details = 'MISSING_RESPONSE';
        }
        verdict = 'correct';
        for (var i = 0; i < submissionResponses.length; i++) {
          if (submissionResponses[i].trim() != expectedResponses[i].trim()) {
            verdict = 'wrong';
            details = 'WRONG_ANSWER';
            break;
          }
        }
        checker.verdict = verdict;
        checker.details = details;
        checker.checkerType = 'automatic';
        checker.taskType = 'answer';
        submission.checker = {};
        submission.checker = checker;
        checkedSubmissions.push(submission);
      }
      else if (targetTask.type == 'code') {
        var verdict = 'none';
        var details = '';
        checker.verdict = verdict;
        checker.details = details;
        checker.checkerType = 'automatic';
        checker.taskType = 'code';
        submission.checker = {};
        submission.checker = checker;
        checkedSubmissions.push(submission);
      }
      else if (targetTask.type == 'function') {
        var code = submission.response[0];
        var task = submission.task;
        var wrapped = await wrapCode(code, task);
        var data = await sails.helpers.runCode.with({code: wrapped});
        var verdict = '';
        var details = '';
        var runResult = null;
        if (data.type == 'success') {
          data.result.stdout = data.result.stdout.split('\n');
          if (data.result.stderr != '') {
            verdict = 'wrong';
            details = 'PROGRAM_ERROR';
          }
          else {
            for (var i = 0; i < data.result.stdout.length; i++) {
              if (data.result.stdout[i].trim() == '') {
                data.result.stdout.splice(i, 1);
                i--;
              }
            }
            verdict = 'correct'
            if (task.sample_io_outputs.length != data.result.stdout.length) {
              verdict = 'wrong';
              details = 'OUTPUT_MISMATCH';
            }
            else {
              for (var i = 0; i < data.result.stdout.length; i++) {
                if (data.result.stdout[i].trim() != task.sample_io_outputs[i].trim()) {
                  verdict = 'wrong';
                  details = 'OUTPUT_MISMATCH';
                  break;
                }
              }
            }
          }
          runResult = {stdout: data.result.stdout, stderr: data.result.stderr}
        }
        else {
          verdict = 'none';
          details = 'RUN_FAIL';
          runResult = {error: data.result }
        }

        checker.verdict = verdict;
        checker.details = details;
        checker.source = data.source;
        checker.checkerType = 'automatic';
        checker.taskType = 'function';
        checker.runResult = runResult;
        submission.checker = {};
        submission.checker = checker;
        checkedSubmissions.push(submission);
      }
    }

    return exits.success(checkedSubmissions);
  }
}
