const { app, BrowserWindow, ipcMain, globalShortcut } = require('electron');
const fs = require('fs');
const ProgressBar = require('electron-progressbar')
const localeData = require('./app_modules/locale.js').data;
const generator = require('./app_modules/generator.js');
const generateReport = require('./app_modules/generateReport.js');

var language = 'jp';

var locale = {};
for (var text in localeData) {
  if (!localeData.hasOwnProperty(text)) continue;
  if (localeData[text][language]) {
    locale[text]  = localeData[text][language];
  }
  else {
    // If no corresponding text was found for the chosen language, "NO TEXT FOUND" is assigned.
    // If you encounter this, consider editing ../app_modules/locale.js to add the text for the chosen language.
    locale[text] = 'NO TEXT FOUND';
  }
}

var menuWindow = null;
var viewerWindow = null;
var windows = [];
var progressBar = null;

function hideAllWindows() {
  for (var i = 0; i < windows.length; i++) {
    windows[i].hide();
  }
}

app.on('ready', function() {
  menuWindow = new BrowserWindow({
    width: 800,
    height: 600,
    center: true,
    resizable: false,
    title: locale.title,
    show: false,
    webPreferences: {
      nodeIntegration: true,
    }
  });
  menuWindow.setMenu(null);
  menuWindow.loadFile('view/menu.html');
  //menuWindow.openDevTools();
  windows.push(menuWindow);

  viewerWindow = new BrowserWindow({
    width: 1150,
    height: 700,
    center: true,
    resizable: false,
    title: locale.title,
    show: false,
    webPreferences: {
      nodeIntegration: true,
    }
  });
  viewerWindow.setMenu(null);
  viewerWindow.loadFile('view/viewer.html');
  // viewerWindow.openDevTools();
  windows.push(viewerWindow);

  menuWindow.once('ready-to-show', function() {
    menuWindow.show();
    menuWindow.webContents.send('initialize-text', locale);
  });

  menuWindow.on('close', function() {
    app.quit();
  });

  viewerWindow.on('close', function() {
    app.quit();
  });
});

ipcMain.on('initiate-generation', async function(event, data) {
  hideAllWindows();
  try {
    var submissions = JSON.parse(fs.readFileSync(data.submissionPath, 'utf-8'));
    var outputDirectory = data.directoryPath;

    var submissionsLength = submissions.submissions.length;
  }
  catch(e) {
    menuWindow.show();
    menuWindow.webContents.send('show-error', e);
    return;
  }

  progressBar = new ProgressBar({
    title: '',
    indeterminate: false,
    text: locale.generating_reports,
    detail: '',
    maxValue: submissions.submissions.length + 1,
    initialValue: 0,
    style: {
      text: {
        'display': 'block',
        'height': '30px'
      }
    },
    browserWindow: {
      webPreferences: {
        nodeIntegration: true
      }
    }
  });

  progressBar.on('completed', function() {
    progressBar.detail = locale.finished;
    menuWindow.show();
    menuWindow.webContents.send('display-message', locale.check_success)
  }).on('aborted', function() {
    menuWindow.show();
  })

  generator.generate(submissions, outputDirectory, progressBar, locale, function(e) {
    progressBar.close();
    menuWindow.webContents.send('display-message', e)
  });
});

ipcMain.on('initiate-view', function(event, data) {
  hideAllWindows();
  try {
    var checkedSubmissions = JSON.parse(fs.readFileSync(data, 'utf-8'))
    viewerWindow.show();
    viewerWindow.webContents.send('initialize-text', locale);
    viewerWindow.webContents.send('initialize-data', {checkedSubmissions: checkedSubmissions});
  }
  catch(e) {
    menuWindow.show();
    menuWindow.webContents.send('display-message', e);
  }
})

ipcMain.on('back-to-menu', function(event, data) {
  hideAllWindows();
  menuWindow.show();
})

ipcMain.on('write-report', async function(event, data) {
  generateReport.execute(data.users, data.tasks, data.submissions, data.fileName, data.flipped).then(function(e) {
    viewerWindow.webContents.send('display-message', locale.write_success);
  }).catch(function(e) {
    viewerWindow.webContents.send('display-message', e);
  });
})

ipcMain.on('display-source', function(event, data) {
  var sourceWindow = new BrowserWindow({
    width: 640,
    height: 480,
    center: true,
    resizable: false,
    title: locale.source,
    show: false,
    webPreferences: {
      nodeIntegration: true,
    }
  });
  sourceWindow.setMenu(null);
  sourceWindow.loadFile('view/source.html');
  // sourceWindow.openDevTools();
  sourceWindow.once('ready-to-show', function() {
    sourceWindow.show();
    sourceWindow.webContents.send('update-data', data);
  })
})
