const alphaSort = require('alpha-sort');
const Excel = require('exceljs')

//*** This code is copyright 2002-2016 by Gavin Kistner, !@phrogz.net
//*** It is covered under the license viewable at http://phrogz.net/JS/_ReuseLicense.txt
//*** Reuse or modification is free provided you abide by the terms of that license.
//*** (Including the first two lines above in your source code satisfies the conditions.)

// Include this code (with notice above ;) in your library; read below for how to use it.

Date.prototype.customFormat = function(formatString){
var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
var dateObject = this;
YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
DD = (D=dateObject.getDate())<10?('0'+D):D;
DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

h=(hhh=dateObject.getHours());
if (h==0) h=24;
if (h>12) h-=12;
hh = h<10?('0'+h):h;
hhhh = hhh<10?('0'+hhh):hhh;
AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
mm=(m=dateObject.getMinutes())<10?('0'+m):m;
ss=(s=dateObject.getSeconds())<10?('0'+s):s;
return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}

async function execute(users, tasks, checkedSubmissions, outputDirectory, type) {
  var taskIds = [];
  var userIds = [];
  var taskList = tasks;
  var userList = users;
  if (!type) {
    type = 'normal';
  }

  for (var i = 0; i < taskList.length; i++) {
    taskIds.push(taskList[i].id);
  }
  for (var i = 0; i < userList.length; i++) {
    userIds.push(userList[i].user_name);
  }

  for (var i = 0; i < userList.length; i++) {
    for (var j = i + 1; j < userList.length; j++) {
      if (userList[i].user_name.localeCompare(userList[j].user_name) > 0) {
        var temp = userList[i];
        userList[i] = userList[j];
        userList[j] = temp;
        temp = userIds[i];
        userIds[i] = userIds[j];
        userIds[j] = temp;
      }
    }
  }
  for (var i = 0; i < taskList.length; i++) {
    for (var j = i + 1; j < taskList.length; j++) {
      if (taskList[i].title.localeCompare(taskList[j].title) > 0) {
        var temp = taskList[i];
        taskList[i] = taskList[j];
        taskList[j] = temp;
        temp = taskIds[i];
        taskIds[i] = taskIds[j];
        taskIds[j] = temp;
      }
    }
  }

  var workbook = new Excel.Workbook();
  workbook.creator = 'funcodesubmission';
  workbook.created = new Date();
  var summarySheet = workbook.addWorksheet('Summary', {properties: {defaultColWidth: 13}});
  var summaryWithTimeSheet = workbook.addWorksheet('Summary With Time', {properties: {defaultColWidth: 13}});
  var summaryWithSubmissionSheet = workbook.addWorksheet('Summary With Submission', {properties: {defaultColWidth: 13}});
  var primaryAxis = taskList.map(a => a.title);
  var secondaryAxis = userIds;
  if (type == 'flipped') {
    var temp = primaryAxis;
    primaryAxis = secondaryAxis;
    secondaryAxis = temp;
  }


  var tasksRow = [];
  tasksRow.push('');
  for (var i = 0; i < primaryAxis.length; i++) {
    tasksRow.push(primaryAxis[i]);
  }
  summarySheet.addRow(tasksRow);

  var tasksRowWithTime = [];
  tasksRowWithTime.push('');
  for (var i = 0; i < primaryAxis.length; i++) {
    tasksRowWithTime.push(primaryAxis[i]);
    tasksRowWithTime.push('');
  }
  summaryWithTimeSheet.addRow(tasksRowWithTime);

  var tasksRowWithSubmission = [];
  tasksRowWithSubmission.push('');
  for (var i = 0; i < primaryAxis.length; i++) {
    tasksRowWithSubmission.push(primaryAxis[i]);
    tasksRowWithSubmission.push('');
  }
  summaryWithSubmissionSheet.addRow(tasksRowWithSubmission);

  var submissionRows = [];
  for (var i = 0; i < secondaryAxis.length; i++) {
    var current = [secondaryAxis[i]];
    for (var j = 0; j < primaryAxis.length; j++) {
      current.push(0);
    }
    submissionRows.push(current);
  }

  var submissionRowsWithTime = [];
  for (var i = 0; i < secondaryAxis.length; i++) {
    var current = [secondaryAxis[i]];
    for (var j = 0; j < primaryAxis.length; j++) {
      current.push(0);
      current.push('');
    }
    submissionRowsWithTime.push(current);
  }

  var submissionRowsWithSubmission = [];
  for (var i = 0; i < secondaryAxis.length; i++) {
    var current = [secondaryAxis[i]];
    for (var j = 0; j < primaryAxis.length; j++) {
      current.push(0);
      current.push('');
    }
    submissionRowsWithSubmission.push(current);
  }

  for (var i = 0; i < checkedSubmissions.length; i++) {
    var currentTask = checkedSubmissions[i].task;
    var currentUser = checkedSubmissions[i].user;

    var taskIndex = taskIds.indexOf(currentTask.id);
    var userIndex = userIds.indexOf(currentUser.user_name);
    if (type == 'flipped') {
      var temp = taskIndex;
      taskIndex = userIndex;
      userIndex = temp;
    }

    var verdict = 'unchecked';
    if (checkedSubmissions[i].checker) {
      verdict = checkedSubmissions[i].checker.verdict;
    }

    if (verdict == 'none') {
      submissionRows[userIndex][taskIndex + 1] = 'skipped';
      submissionRowsWithTime[userIndex][taskIndex * 2 + 1] = 'skipped';
      submissionRowsWithTime[userIndex][taskIndex * 2 + 2] = new Date(checkedSubmissions[i].updatedAt).customFormat('#hh#:#mm#:#ss#\r\n#MM#/#DD#/#YYYY#');
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 1] = 'skipped';
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 2] = '';
    }
    else if (verdict == 'correct') {
      submissionRows[userIndex][taskIndex + 1] = 2;
      submissionRowsWithTime[userIndex][taskIndex * 2 + 1] = 2;
      submissionRowsWithTime[userIndex][taskIndex * 2 + 2] = new Date(checkedSubmissions[i].updatedAt).customFormat('#hh#:#mm#:#ss#\r\n#MM#/#DD#/#YYYY#');
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 1] = 2;
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 2] = checkedSubmissions[i].response.join('\n');
    }
    else if (verdict == 'wrong') {
      submissionRows[userIndex][taskIndex + 1] = 1;
      submissionRowsWithTime[userIndex][taskIndex * 2 + 1] = 1;
      submissionRowsWithTime[userIndex][taskIndex * 2 + 2] = new Date(checkedSubmissions[i].updatedAt).customFormat('#hh#:#mm#:#ss#\r\n#MM#/#DD#/#YYYY#');
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 1] = 1;
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 2] = checkedSubmissions[i].response.join('\n');
    }
    else if (verdict == 'unchecked') {
      submissionRows[userIndex][taskIndex + 1] = 'unchecked';
      submissionRowsWithTime[userIndex][taskIndex * 2 + 1] = 'unchecked';
      submissionRowsWithTime[userIndex][taskIndex * 2 + 2] = new Date(checkedSubmissions[i].updatedAt).customFormat('#hh#:#mm#:#ss#\r\n#MM#/#DD#/#YYYY#');
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 1] = 'unchecked';
      submissionRowsWithSubmission[userIndex][taskIndex * 2 + 2] = checkedSubmissions[i].response.join('\n');
    }

  }

  for (var i = 0; i < submissionRows.length; i++) {
    summarySheet.addRow(submissionRows[i]);
    summaryWithTimeSheet.addRow(submissionRowsWithTime[i]);
    summaryWithSubmissionSheet.addRow(submissionRowsWithSubmission[i]);
  }

  for (var i = 0; i < userIds.length + 1; i++) {
    var row1 = summarySheet.getRow(i + 1);
    var row2 = summaryWithTimeSheet.getRow(i + 1);
    var row3 = summaryWithSubmissionSheet.getRow(i + 1);
    for (var j = 0; j < primaryAxis.length * 3; j++) {
      row1.getCell(j + 1).alignment = { wrapText: true }
      row2.getCell(j + 1).alignment = { wrapText: true }
      row3.getCell(j + 1).alignment = { wrapText: true }
    }
  }

  try {
    await workbook.xlsx.writeFile(outputDirectory);
  }
  catch(e) {
    throw new Error(e);
  }

}

module.exports = {execute};
