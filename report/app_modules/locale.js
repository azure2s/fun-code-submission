data = {
  title: {
    en: 'Submissions Tool',
    jp: '提出ツール'
  },
  instructions: {
    en: 'Select the submissions file and click "Check" to start automatic checking.',
    jp: '提出ファイルを選択し、「チェックする」をクリックして自動チェックを開始します。'
  },
  instructions_2: {
    en: 'Click here if you want to view a submissions file.',
    jp: '提出ファイルを表示するには、ここをクリックしてください。'
  },
  submission_file: {
    en: 'Submissions file (.json)',
    jp: '提出ファイル（.json）'
  },
  output_directory: {
    en: 'Output Directory (output will be saved as submissions-checked.json)',
    jp: '出力ディレクトリ (出力はsubmissions-checked.jsonとして保存されます)'
  },
  browse: {
    en: 'Browse',
    jp: 'ブラウズ'
  },
  generate: {
    en: 'Check',
    jp: 'チェックする'
  },
  view_submissions: {
    en: 'View Submissions',
    jp: '提出物を見る'
  },
  error_missing_fields: {
    en: 'Error: some fields are missing.',
    jp: 'エラー：一部のフィールドが欠落しています。'
  },
  generating_reports: {
    en: 'Checking submissions and generating reports...',
    jp: '送信内容を確認してレポートを生成しています...'
  },
  finished: {
    en: 'Finished!',
    jp: '完成しました！'
  },
  checking_submission: {
    en: 'Checking submission:',
    jp: '提出を確認しています:'
  },
  back: {
    en: 'Back',
    jp: '戻る'
  },
  input: {
    en: 'Input',
    jp: '入力'
  },
  expected: {
    en: 'Expected',
    jp: '期待される出力'
  },
  output: {
    en: 'Output',
    jp: '出力'
  },
  question: {
    en: 'Question',
    jp: '質問'
  },
  time_submitted: {
    en: 'Time Submitted:',
    jp: '提出時間：'
  },
  error: {
    en: 'Error',
    jp: 'エラー'
  },
  generate_excel: {
    eb: 'Generate Excel Report',
    jp: 'Excelレポートを生成'
  },
  write_success: {
    en: 'Report Successfully Saved.',
    jp: 'レポートを保存しました。'
  },
  check_success: {
    en: 'Check successful.',
    jp: 'チェック成功。'
  },
  source: {
    en: 'Source',
    jp: 'ソースコード'
  },
  view_source: {
    en: 'View Source',
    jp: 'ソースコードを見る'
  },
  accounts_text: {
    en: 'Students List',
    jp: '学生リスト'
  },
  tasks_text: {
    en: 'Problems List',
    jp: '問題リスト'
  }
}

module.exports = {data};
