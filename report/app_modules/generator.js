const replaceall = require('replaceall')
const wrapCode = require('./wrapCode.js')
const runCode = require('./runCode.js');
const fs = require('fs');

function checkCode(code, task, callback) {
  var wrapped = wrapCode.execute(code, task);
  var output = runCode.execute(wrapped, callback);
}

async function generate(submissions, outputDirectory, progressBar, locale, failure) {
  var users = submissions.users;
  var tasks = submissions.tasks;
  submissions = submissions.submissions;
  var submissionsLength = submissions.length;
  var checkedSubmissions = [];

  async function processSubmission(submission, callback) {
    progressBar.detail = locale.checking_submission + ' (' + (progressBar.value + 1) + '/' + submissionsLength + ')';
    setTimeout(function() {
      try {
        var targetTask = submission.task;
        var checker = {};
        if (targetTask.type == 'answer') {
          var verdict  = 'none'
          var details = '';
          var submissionResponses = submission.response;
          var expectedResponses = targetTask.question_answers;
          if (submissionResponses.length != expectedResponses.length) {
            verdict = 'wrong';
            details = 'MISSING_RESPONSE';
          }
          verdict = 'correct';
          for (var i = 0; i < submissionResponses.length; i++) {
            if (submissionResponses[i].trim() != expectedResponses[i].trim()) {
              verdict = 'wrong';
              details = 'WRONG_ANSWER';
              break;
            }
          }
          checker.verdict = verdict;
          checker.details = details;
          checker.checkerType = 'automatic';
          checker.taskType = 'answer';
          pushChecker(checker);
        }
        else if (targetTask.type == 'code') {
          var verdict = 'none';
          var details = '';
          checker.verdict = verdict;
          checker.details = details;
          checker.checkerType = 'automatic';
          checker.taskType = 'code';
          pushChecker(checker);
        }
        else if (targetTask.type == 'function') {
          var code = submission.response[0];
          var task = submission.task;
          var result = checkCode(code, task, function(data) {
            var verdict = '';
            var details = '';
            var runResult = null;
            if (data.type == 'success') {
              data.result.stdout = data.result.stdout.split('\n');
              if (data.result.stderr != '') {
                verdict = 'wrong';
                details = 'PROGRAM_ERROR';
              }
              else {
                for (var i = 0; i < data.result.stdout.length; i++) {
                  if (data.result.stdout[i].trim() == '') {
                    data.result.stdout.splice(i, 1);
                    i--;
                  }
                }
                verdict = 'correct'
                if (task.sample_io_outputs.length != data.result.stdout.length) {
                  verdict = 'wrong';
                  details = 'OUTPUT_MISMATCH';
                }
                else {
                  for (var i = 0; i < data.result.stdout.length; i++) {
                    if (data.result.stdout[i].trim() != task.sample_io_outputs[i].trim()) {
                      verdict = 'wrong';
                      details = 'OUTPUT_MISMATCH';
                      break;
                    }
                  }
                }
              }
              runResult = {stdout: data.result.stdout, stderr: data.result.stderr}
            }
            else {
              verdict = 'none';
              details = 'RUN_FAIL';
              runResult = {error: data.result }
            }

            checker.verdict = verdict;
            checker.details = details;
            checker.source = data.source;
            checker.checkerType = 'automatic';
            checker.taskType = 'function';
            checker.runResult = runResult;
            pushChecker(checker);
          });
        }

        function pushChecker(checkerObj) {
          console.log(checkerObj);
          progressBar.value += 1;
          submission.checker = {};
          submission.checker = checkerObj;
          checkedSubmissions.push(submission);
          callback();
        }
      }
      catch(e) {
        failure(e);
      }
    }, 50);
  }

  async function processNext() {
    if (submissions.length != 0) {
       await processSubmission(submissions.splice(0,1)[0], function() {
         processNext().catch(function(e) {
           failure(e);
         });
       }).catch(function(e) {
         failure(e);
       });
    }
    else {
      var result = {users: users, tasks: tasks, submissions: checkedSubmissions};
      fs.writeFileSync(outputDirectory + '/' + 'submissions-checked.json', JSON.stringify(result), 'utf-8');
      progressBar.value += 1;
    }
  }

  processNext().catch(function(e) {
    failure(e);
  });
}

module.exports = {generate};
