const { java } = require('compile-run')

function execute(code, callback) {
  let resultPromise = java.runSource(code);
  resultPromise.then(result => {
    callback({type: 'success', source: code, result: result});
  }).catch(e => {
    callback({type: 'error', source: code, result: e});
  });
}

module.exports = {execute}
